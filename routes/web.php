<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ---------------------------------------
/ Front controller
--------------------------------------- */
Route::get('/', 'HomeController@index')->name('home');
Route::get('/commitment', 'HomeController@commitment')->name('commitment');
Route::post('/opinion', 'OpinionController@store')->name('opinion');
Route::get('/success', 'OpinionController@success')->name('success');
Route::get('/get-subtype', 'OpinionController@get_subtype');

/* ---------------------------------------
/ Backend controller
--------------------------------------- */
Auth::routes([
    'verify' => false,
    'register' => false,
    'reset' => false,
    'password.confirm' => false,
]);

Route::resource('/opinions', 'Backend\OpinionController');
Route::post('/opinion-note-add', 'Backend\OpinionController@note_add');
Route::get('/opinion-pdf', 'Backend\PdfController@opinion_pdf');

//to clear all cache
Route::get('__clear',function(){
    try{
        \Artisan::call('cache:clear');
        \Artisan::call('view:clear');
        \Artisan::call('config:clear');
        \Artisan::call('config:cache');
        \Artisan::call('route:clear');
    }
    catch(\Exception $e){
        echo $e->getMessage();
    }
});