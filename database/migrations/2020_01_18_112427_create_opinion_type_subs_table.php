<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpinionTypeSubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opinion_type_subs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('opinion_type_id')->comment('primary kew of opinion_types table');
            $table->string('name');
            $table->unsignedInteger('sort');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opinion_type_subs');
    }
}
