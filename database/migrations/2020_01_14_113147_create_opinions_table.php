<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpinionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opinions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email')->nullable();
            $table->string('age')->nullable();
            $table->unsignedTinyInteger('gender')->default(1)->comment('1=male,2=female,3=transgender');
            $table->string('profession')->nullable();
            $table->string('ward')->nullable();
            $table->string('address')->nullable();
            $table->unsignedInteger('type')->default(0)->comment('primary key of opinion_types table');
            $table->unsignedInteger('sub_type')->default(0)->nullable()->comment('primary key of opinion_type_subs table');
            $table->string('year')->nullable();
            $table->text('opinion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opinions');
    }
}
