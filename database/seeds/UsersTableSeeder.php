<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin user',
            'email' => 'admin@cloudnextltd.com',
            'password' => bcrypt('#cloudnext12345'),
        ]);
    }
}
