<?php

use Illuminate\Database\Seeder;

class OpinionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('opinion_types')->insert([
            [
                'name' => 'সাল ভিত্তিক',
                'sort' => 0,
            ],
            [
                'name' => 'ঐতিহ্যের ঢাকা',
                'sort' => 1,
            ],
            [
                'name' => 'সুন্দর ঢাকা',
                'sort' => 2,
            ],
            [
                'name' => 'সচল ঢাকা',
                'sort' => 3,
            ],
            [
                'name' => 'সুশাসিত ঢাকা',
                'sort' => 4,
            ],
            [
                'name' => 'উন্নত ঢাকা',
                'sort' => 5,
            ],
            // [
            //     'name' => 'ডেঙ্গু সম্পর্কিত',
            //     'sort' => 5,
            // ],
            // [
            //     'name' => 'বর্জ্য ব্যবস্থাপনা',
            //     'sort' => 6,
            // ]
        ]);
    }
}
