### AMADER DHAKA 
[amaderdhaka.com](https://amaderdhaka.com.bd/),  

#### Serve Info  
* Server IP: `157.245.61.230`
* User: `epos`
* SSH Key passphrase: `$Epos!9874123`

#### MySQL  
* User: `root`  
* password: `!CloudSQL@9874123`

#### Git Clone  
`git clone https://Cloudnextgeneration@bitbucket.org/Cloudnextgeneration/amaderdhaka.git`

#### Database  
`CREATE DATABASE amader_dhaka;`  
`GRANT ALL ON amader_dhaka.* TO 'cloudnext'@'localhost' IDENTIFIED BY '!mySQL@123' WITH GRANT OPTION;`

#### Nginx Configuration  
`sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/amaderdhaka.com.bd`  
`sudo nano /etc/nginx/sites-available/amaderdhaka.com.bd`  
`sudo ln -s /etc/nginx/sites-available/amaderdhaka.com.bd /etc/nginx/sites-enabled/`
###### Unlink Config
`sudo unlink /etc/nginx/sites-enabled/default`
###### Test Nginx
`sudo nginx -t`

#### Directory Permission  
`sudo chgrp -R www-data storage bootstrap/cache`  
`sudo chmod -R ug+rwx storage bootstrap/cache`

#### Restart PHP Fpm & Nginx
`sudo systemctl restart php7.2-fpm`  
`sudo systemctl restart nginx`

#### Now navigate the url 🙂