-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 27, 2020 at 09:07 AM
-- Server version: 5.5.61-38.13-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amaderdh_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(15, '2014_10_12_000000_create_users_table', 1),
(16, '2014_10_12_100000_create_password_resets_table', 1),
(17, '2019_08_19_000000_create_failed_jobs_table', 1),
(18, '2020_01_14_113147_create_opinions_table', 1),
(19, '2020_01_15_053210_create_opinion_types_table', 1),
(20, '2020_01_15_103132_create_notes_table', 1),
(21, '2020_01_18_112427_create_opinion_type_subs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `opinion_id` int(10) UNSIGNED NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `opinions`
--

CREATE TABLE `opinions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1=male,2=female,3=transgender',
  `profession` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ward` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'primary key of opinion_types table',
  `sub_type` int(10) UNSIGNED DEFAULT '0' COMMENT 'primary key of opinion_type_subs table',
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opinion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `opinions`
--

INSERT INTO `opinions` (`id`, `name`, `phone`, `email`, `age`, `gender`, `profession`, `ward`, `address`, `type`, `sub_type`, `year`, `opinion`, `created_at`, `updated_at`) VALUES
(1, 'Shohagh Golzar', '01707011310', NULL, '25', 1, 'Business', '17', '97 Bashir Uddin Road. Kalabagan, Dhaka', 4, 13, NULL, 'মতামত', '2020-01-19 17:42:13', '2020-01-19 17:42:13'),
(2, 'জনি', '০১৩০২৫৫৬২৭৯', 'mahfujjoney24@gmail.com', '30', 1, 'ami 12 no.ward awami volunteers league er vice president.', '12', '19/g,Shanti Tower,Ansarcamp,mirpur 1', 2, 4, NULL, 'Vaiya, foot path guli hawkar der dokhole...\r\namra kisu bolle boley,\"amra police re taka dei\".\r\namra thakbo...\r\nbisoy ta dekhben ki??', '2020-01-21 09:16:41', '2020-01-21 09:16:41'),
(3, 'adfgchcv', '0215498869', NULL, NULL, 1, NULL, NULL, NULL, 3, 14, NULL, 'hbhibklbn', '2020-01-21 19:16:58', '2020-01-21 19:16:58'),
(4, 'মোঃ আবু সাঈদ', '০১৮৮৭৪৪৪৫১৫', 'sayeedmamunabu@gmail.com', '42', 1, 'বেকার', '38', 'মধ্যে বাড্ডা আর্দশ নগর', 2, 6, NULL, 'জিতবে আবার নৌকা', '2020-01-22 14:36:26', '2020-01-22 14:36:26'),
(5, 'মোঃ আবু সাঈদ', '০১৮৮৭৪৪৪৫১৫', 'sayeedmamunabu@gmail.com', '42', 1, 'বেকার', '38', 'মধ্যে বাড্ডা আর্দশ নগর', 2, 6, NULL, 'জিতবে আবার নৌকা', '2020-01-22 14:36:28', '2020-01-22 14:36:28'),
(6, 'মোঃ আবু সাঈদ', '০১৮৮৭৪৪৪৫১৫', 'sayeedmamunabu@gmail.com', '42', 1, 'বেকার', '38', 'মধ্যে বাড্ডা আর্দশ নগর', 2, 6, NULL, 'জিতবে আবার নৌকা', '2020-01-22 14:36:47', '2020-01-22 14:36:47'),
(7, 'Sultan', '01612888140', 'sultan72nahid@gmail.com', '49', 1, 'Self employed', '22', '31/1b vagolpur hazaribagh', 2, 6, NULL, 'Best mayor', '2020-01-22 23:57:42', '2020-01-22 23:57:42'),
(8, 'প্রযোজ্য নহে', '০০০০০০০', NULL, NULL, 1, NULL, NULL, NULL, 2, 6, NULL, 'বাড়িভাড়া নিয়ন্ত্রন আইন চাই।  অগ্রীম এর নামে অতিরিক্ত টাকা দেওয়ার প্রচলন বন্ধ করা জরুরি। এই ঢাকাতে সারা বাংলাদেশের কোটি মানুষ থাকে তাই তাদেরকে বাদ দিয়ে কোন উন্নয়নই ফলপ্রসু হবেনা।\r\nব্যাবসার নামে ডাকাতি বন্ধ করতে হবে। পর্যাপ্ত গনশৌচাগার প্রয়োজন। রিক্সার সংখ্যা কমিয়ে গণপরিবহন প্রচলন সহ নাগরিক নিরাপত্তা নিশ্চিত জরুরী।\r\nধন্যবাদ।', '2020-01-23 16:46:39', '2020-01-23 16:46:39'),
(9, 'রেহানা আক্তার', '০১৭১১০৩১০০২', NULL, NULL, 1, NULL, NULL, NULL, 2, 3, NULL, 'তাপস ভাই, \r\nআসসালামুআলাইকু,  আপনার এই পেজটি খুব ভাল করে পড়েছি। পড়ে আমার মনে হলো একটি রাষ্ট্রের সার্বিক উন্নয়ন করতে  হলে যে যে বিষয়গুলো নিয়ে কাজ করতে হয়  তা সবকিছুই আপনার লেখার মধ্যে পেয়েছি। তারপরও বলতে চাই \" clean & beautiful dhaka \" কে নুতন করে সাজাতে হলে অবশ্যই পর্যটন এর উন্নয়ন  বিষয়ে একটি যুগোপযোগী মাস্টার প্ল্যান করে এগুতে হবে। বিশ্বের বহু দেশ আজ পর্যটন নির্ভর। পর্যটন এমন একটি সংস্কৃতি বা ঐতিহ্য যা দেশকে উপস্থাপন করে বিশ্বের দরবা।    ঢাকা আমাদের রাজধানী।  এই রাজধানীতে বড় বড় বিল্ডিংয়ের ইট সুরকি লোহা ছাড়া নান্দনিক তেমন কিছু আমাদের চোখে পড়েনা। তবে ইচ্ছে করলেই যে ভাল কিছু করা যায় তার নমুনা আমরা দেখছি \" হাতির ঝিল \"।  সো,  আপনি পুরাতন ঢাকার যে হেরিটেজগুলো আছে তা নিয়ে ভাবতে পারেন। আধুনিক সংস্কারের মাধ্যমে এই হেরিটেজগুলো হাজার বছর বাঁচিয়ে রাখা সম্ভব।  আমাদের পরবর্তী প্রজন্ম এই হেরিটেজগুলোর মাধ্যমে খুঁজে ফিরবে ইতিহাসকে জানার অনেক কিছু।  আপনিই পারবেন কারণ আপনার ক্লিন ইমেজ,  মেধা, দক্ষতা, দূরদর্শিতা   সবকিছুই একেবারেই উন্নয়ন বান্ধব।  তাই বঙ্গবন্ধুর স্বপ্ন এবং দেশরত্ন শেখ হাসিনার সরকারের হাতকে শক্তিশালী  তথা এই ঢাকার এই জনগণের আমোঘ স্বপ্নকে বাস্তবায়ন করবেন, এটাই আমাদের প্রত্যাশা। আপনার উন্নয়নের কাজে আমাদের শরিক করতে চাইলে অবশ্যই আপনার কাজে আমরা আপনার পাশে থাকবো।  ডাকলেই আমাদের আপনার পাশে পাবেন। পরিশেষে  আপনার সুস্বাস্থ্য ও  দীর্ঘায়ু কামনা করছি। \r\nধন্যবাদান্তে, রেহানা আক্তার,  স্টুডেন্ট ওয়েলফেয়ার এডভাইজার, স্টামফোর্ড ইউনিভার্সিটি বাংলাদে।', '2020-01-23 17:28:04', '2020-01-23 17:28:04'),
(10, 'Sayed rais hossen', '01676088769', 'raiseee14@gmail.com', '29', 1, 'Service', NULL, 'Banasree,', 2, 6, NULL, 'স্যার, আপনার সততায় আমি সত্যি মুগ্ধ, আওয়ামী লীগের আন্ধ ভক্ত, আনেকে বলে জন্ম সুত্রে আওয়ামী,  আপনার কাছে আমার একটাই চাওয়া বনশ্রী হতে ভিতর দিয়ে ভুইয়া পাড়া, গোড়ান, খিলগাঁও, হয়ে সহজে মতিঝিল যাওয়া যায়, কিন্তু ওই রাস্তার বেহাল অবস্থা  দেখার কেউ নাই, আমি চাই আপনি এই বছরের মধ্যে একটা সমাধান করে দিবেন, আর আপনার জয় সুনিশ্চিত।', '2020-01-23 20:11:01', '2020-01-23 20:11:01'),
(11, 'আলহাজ্ব মোঃ কামাল পাশা,38 নং ওয়ার্ড,ঢাকা', '01711698864', NULL, '65', 1, 'ব্যবসা 38', '38', '9 জরিয়াটুলি লেন, নবাবপুর,ঢাকা।', 5, 1, NULL, 'ইনশাআল্লাহ, জননেএী শেখ হাসিনার আপনার উপর আস্থা রেখেছে বলেই আপনাকে প্রার্থী করেছেন। জননেএীর উন্নয়নের ছোঁয়া ঢাকা বাসী আপনার মাধ্যমে পাবে এই দোয়া করি।', '2020-01-23 21:06:20', '2020-01-23 21:06:20'),
(12, 'মোহাম্মদ সাজ্জাদুল ইসলাম', '01711507045', 'shiyamsunny@gmail.com', '38', 1, 'ফ্রিলেন্সার', '17', '৭১/১ শুক্রাবাদ, ঢাকা ১২০৭', 5, 29, NULL, 'প্রতিটি ward এ একটি করে service desk করা হবে, উক্ত ডেস্ক এ একজন করে প্রতিনিধি বসবেন। তিনি থাকবেন সরাসরি City Corporation  এর কল সেন্টারের অধিন এ। অবশ্যই কাউন্সিলর এর অধিনে নয়। এই ডেস্ক এ যিনি বসবেন তার বেতন সিটি করপোরেশন দিতে পারে, না পারলে সিমিত সারভিস ফি জনগন থেকে নেওয়া যেতে পারে বা অন্য খাত পাওয়া যাবেই।   তার কাজের ধরন হবে মেয়র, কল সেন্টার আর জনগনের সাথে এক টি ব্রিজ এর মত। অনেকটা মোবাইল সেবা দাতা দের কাস্টমার সারভিস সেন্টার এর মত আর কাউন্সিলর সাহেব থাকবেন dimand and execution part এ।   এই খাতের খরচ টি লাভজনক করা সম্ভব। আর সাবসিডি দিতে পারলে ত কথাই নেই। এই wing টি হবে মেয়র এর direct feedback and quality monitoring এর একটি মাদ্ধম। আর এর এজেন্ট রা থাকবে প্রশিক্ষিত আর প্রফেশনাল।     \r\n\r\nবিস্তারিত জানতে চাইলে আমাকে কল করুন। হয়তো অল্প কথায় বোঝাতে নাও পারতে পারি।', '2020-01-23 21:31:49', '2020-01-23 21:31:49'),
(13, 'নাসরিন নিহার।', '01711281745', NULL, NULL, 2, 'পত্রিকার সম্পাদক', NULL, 'কলাবাগান, ঢাকা।', 6, 32, NULL, '*মানুষের মাঝে মানবিক মূল্যবোধ ফিরিয়ে আনার প্রত্যয়ে কাজ করতে হবে। *শিক্ষা প্রতিষ্ঠানে ধনীগরিবের বৈষম্য থাকবে না। ধনী গরিবের জন্য শিক্ষার মান হবে এক এবং অভিন্ন।\r\n*পথশিশুদের কোনো প্রকার রাজনৈতিক মিটিং মিছিলে কাজে লাগানো যাবে না। তাদের জন্য অন্ন, বস্ত্র, শিক্ষা, চিকিৎসা, বাস্থানের ব্যবস্থা করতে হবে। ন\r\n*সকল প্রকার নেশাদ্রব্য বা মাদক কেনা বেচাকারিদের কঠোর শাস্তির ব্যবস্থা করতে হবে।\r\n* পরিকল্পিতভাবে অবকাঠামো নির্মান আইন করতে হবে। আইনের বাইরে কেউ যদি অবকাঠামো নির্মাণ করেন, তার বিরুদ্ধে কঠোর শাস্তির ব্যবস্থা করতে হবে। \r\n*যানজট নিরাসনের আইন করতে হবে এবং তা দ্রুত বাস্তবায়ন করতে হবে।\r\n* পথচারিদের জন্য বেশি বেশি শৌচাগারের ব্যবস্থা করতে হবে এবং নারীদের জন্্য রাস্তার পাশে, বাজার ঘাটি, শপিংমলে নারী বান্ধব টয়লেটের ব্যবস্থা করতে হবে। \r\n* বস্তিবাসীদের জন্্য স্বমুল্যে আবাসনের ব্যবস্থা করতে হবে।\r\n*নারী ও শিশু নিরাপত্তা জ্বোরদার করতে হবে।\r\n* দুষণমুক্ত পরিবেশ গড়তে হবে।\r\n* ছিনতায়, সন্ত্রাস, চাঁবাজি বন্ধ করতে হবে।\r\n* প্রতিটি মহল্লায় স্পাহে অথবা মাসে একবার হলেও মানুষদের জন্য নীতি শিক্ষার ব্যবস্থা করতে হবে। \r\n* বৃদ্ধ,অনাথ, অসহায় বিধবাদের জীবন যাপনের ব্যবস্থা করতে হবে।\r\n*তরুণদের দেশপ্রেমে উদ্বু্দ্ধ করতে হবে।\r\n*সৎ ও আদর্শবান মানুষদের সাথে নিয়ে উন্নত ঢাকা গড়ার কাজ করতে হবে।\r\n* কোনো ক্রমেই দুর্নীতিকে প্রশ্রয় দেয়া যাবে না। \r\n\r\nপ্রত্যাশা  করছি, মেয়র পদে নির্বাচিত হয়ে আগামী দিনে আপনি আমাদের  সুন্দর টলটলে পরিচ্ছন্ন এক ঢাকা উপহার দিবেন। আপনার সার্বিক সাফল্য কামনা করছি।', '2020-01-24 00:15:56', '2020-01-24 00:15:56'),
(14, 'আরিকুল ইসলাম মিনিং', '০১৯১২৪৯৭৪৪১', NULL, '25', 1, 'চাকুরীজীবী', '59', 'মোহাম্মদবাগ   মেরাজ নগর কদমতলী ঢাকা', 6, 34, NULL, 'আল্লাহকে হাজির-নাজির করে আমি চাই দেশের উন্নতি দশের উননতি জাতির উন্নতি', '2020-01-24 01:45:35', '2020-01-24 01:45:35'),
(15, 'Mohammed Ali', '01886451800', 'ebrahimkhalil190310@gmail.com', '35', 1, 'Private service', '43', '20no,Justice Lal Mohan Das Lane.Shutrapur.Dhaka-1100', 2, 6, NULL, 'স্যার, পুরনো ঢাকাবাসি হিসেবে আমার মনে হয় প্রথমেই রাস্তার উপরের কেবলগুলো(ইলেকট্রিক তার,ডিশ এর তার,ইন্টারনেটের তার সহ ইত্যাদি ইত্যাদি)রাস্তার নীচ দিয়ে বা যুগোপযোগী কোন বিকল্প পথের ব্যবস্থা করা, রাস্তাগুলো মান-সম্মত ভাবে মেরামত করা, পানি ও পয়নিস্কাশন এর সঠিক ব্যবস্থা নিশ্চিত করা এবং  অবশ্যই পরিষ্কার-পরিচ্ছন্নতা (প্রয়োজনে জরিমানা পদ্ধতির মাধ্যমে হলেও) নিশ্চিত করতে হবে... তবে সবচেয়ে গুরুত্বপূর্ণ বিষয় হলো  নিষ্ঠা ও ধৈর্যেের সাথে এ সম্পূন বিষয়টাকে তদারকি করা।।', '2020-01-24 03:26:24', '2020-01-24 03:26:24'),
(16, 'মোরশেদা বেগম লাকী', '০১৭১৫০৭৭৭৭২', NULL, '53', 2, 'আইনজীবী', '15', '\'আদেল কর্নার\' ফ্লাট এ- ১ ,  বাসা ৮-এ/৫- এ,  রোড নং ১৪( নতুন) ধানমন্ডি  আ/ এ , ঢাকা ।', 2, 1, NULL, '১ / প্রতিদিন ঝাড়ুদাররা ঠিকমত ঝাড়ু দেয় না। কোন রকম ঝাড়ুর মাথা দিয়ে অল্প কিছু ময়লা তুলে নেয় ।  আমার মতামত প্রতিটি ওয়ার্ডের প্রতিনিধির উপর দায়িত্ব  দেবেন , তারা প্রতিদিন পর্যোবেক্ষন করবেন । এমন ভাবে ঝাড়ু দিবে যেন  ধূলাবালি  সহ সকল ময়লা তুলে নেয়।। তাহলে দেখবেন ঢাকায় এ্যাজমায় আক্রান্ত  , কাশিতে  আক্রান্ত  রোগীর  সংখা  অনেক কমে যাবে এবং ঢাকা একটা পরিস্কার পরিচ্ছন্ন  নগরী তে পরিনত হবে ।এটা খুবই জরুরী ।এটা প্রথমেই  করতে পারলে মানুষ সকাল বলা বাহিরে বের হলেই রাস্তাঘাট পরিস্কার  দেখলেই মনটা ভরে যাবে এবং প্রথমেই মন্তব্য  করবেন \" \" সার্থক  মেয়র , সার্থক  ঢাকা বাসী  \"।। \r\nআজ এই ১ টা প্রথম মতামত দিলাম , বাকী মতামত  আমি পর্যায়ক্রমে  দেব ।।', '2020-01-24 04:12:40', '2020-01-24 04:12:40'),
(17, 'মুশফিক উদ্দিন', '+৮৮০১৮৭২২৭১০২১', 'mushfiq.uddin1122@gmail.com', '17', 1, 'ছাত্র', '22', 'নবীপুরলেন,হাজারীবাগ,ঢাকা-১২০৯', 6, 34, NULL, 'ঢাকা আমাদের বাসস্থান কিন্তু ঢাকার পরিচালনায় যাদের আমরা ভরসা করি তাদের প্রধান কর্তব্য জনগণ এর জন্য কাজ করা। আমি একটু ভিন্ন মতামত দিবো। ঢাকার কিছু রাস্তা বৃষ্টি পড়লে কাদার ফলে আর হাটার যোগ্য থাকেনা! আর জলাবদ্ধতা তো আছেই! তাই আমি মনে করি এই সমস্যা দ্রুত সমাধান করা উচিত।আর কাউন্সিলর\'রা ঠিক মতো কাজ করছে নাকি তাও নজরে রাখতে হবে।কোনো প্রকল্প এর কাজ শুরু করলে কাদের দিয়ে কাজ করানো হচ্ছে এবং কাজের মান কেমন তাও নজরে রাখতে হবে।জনগণ অনেক কষ্ট করে টাকা উপার্জন করে,তাদের টাকা দুর্নীতে নষ্ট হলে নিশ্চই সৃষ্টিকর্তা মাফ করবেননা!\r\nধন্যবাদ।', '2020-01-24 05:00:19', '2020-01-24 05:00:19'),
(18, 'আজিজ সরকার', '০১৭৫২৮৮২১৩১', NULL, '50', 1, 'বেকার', '38', '১৩২/১ বি সি সিরোড ঠাটারী বাজার', 6, 32, NULL, 'পতিটা ওয়াডে এক জন করে সি ই ডি রাখতে হবে কারন নগর ভবনের কমচারিরা ঠিক মতন ঠিওটি করে কি না পতিটা ওয়াডে  এটা আমার অনুরুদ \r\nতা না হলে লাখ টাকার বাগান খাবে দের টাকার ছাগলে', '2020-01-24 09:12:21', '2020-01-24 09:12:21'),
(19, 'আজিজ সরকার', '০১৭৫২৮৮২১৩১', NULL, '50', 1, 'বেকার', '38', '১৩২/১ বি সি সি রোড ঠাটারিবাজার', 6, 32, NULL, 'আমার এলাকার সমস্সা আমি জানি', '2020-01-24 09:19:49', '2020-01-24 09:19:49'),
(20, 'Jawad Ahmed', '01913380527', 'jawad.khandaker@gmail.com', '29', 1, 'Business', '16', '51 Circular Road Hatirpool Dhaka', 2, 6, NULL, 'Ekti route e ekti companyr bus er dabi janachhi, bus stoppage Chara bus thambe na, jebhabe prithibir onnanno desher under e bus chole thake, rickshaw komiye fela uchit, ebong nirdishto elaka bhittik rickshaw thaka uchit, jebhabe gulshan banani baridhara te rickshaw choleche, aro gaach ddekhte chai! Jekhanei faka jayga ache gaach Jano lagano hoy', '2020-01-24 09:31:54', '2020-01-24 09:31:54'),
(21, 'Faysal Amin', '01912295907', NULL, NULL, 1, NULL, NULL, NULL, 4, 22, NULL, 'অনেক কাজ আছে, অনেক কাজ করতে পারবেন, আপনি শুধু অবৈধ পার্কিং বন্ধ করে এ্যাম্বুলেন্স, ফায়ার সার্ভিস, পুলিশ এর গাড়ির জন্য আলাদা লেন করে দিন, প্রয়োজনে আইন করে দিন সে লেন এ অন্য কোন যানবাহন চলতে পারবে না। শুধু এটা বাস্তবায়ন করেন, সারা দেশের মানুষ আপনাকে কেয়ামত পর্যন্ত মনে রাখবে', '2020-01-24 11:01:32', '2020-01-24 11:01:32'),
(22, 'tareq', '0100000', NULL, NULL, 1, NULL, NULL, NULL, 2, 6, NULL, 'hgfdxfgcbjkh', '2020-01-24 18:26:40', '2020-01-24 18:26:40'),
(23, 'সাহেল', '01911226688', 'sanjidalabiba@gmail.com', '39', 1, 'ব্যাবসা', '18', '১০,সেন্ট্রাল রোড, নিউমার্কে, ঢাকা-১২০৫', 5, 20, NULL, 'প্রতিটা ওয়ার্ডে পর্যাপ্ত পরিমান আলোর ব্যাবস্থা করে স্থানীয় লোকের আলোচনার ভিত্তিতে বিভিন্ন  পয়েন্টে হিডেন সি,সি ক্যামেরা স্থাপন করা যা মনিটরিং কন্ট্রোলার কাউন্সিল,সেই সাথে এলাকার স্থানীয় বিভিন্ন সুশিল সমাজের ৭-১৩ সদস্য বিশিষ্ট কমিটি ও স্থানীয় থানা এবং নগর ভবনে ফুটেজ সংরক্ষিত থাকবে এর সমন্নয় ঘটিয়ে অনেকাংশে নারীপুরুষ শিশু সহ সকল    মাদক,অপরাধ,যত্রতত্র স্থানে ময়লা ফেলা রোধ, ডেংগু সচেতনতায় কর্মকান্ড কর্ম তদারকি, সিটিকর্পোরেশন বিদ্যুৎ, ঝাড়ু বিভিন্ন কাজে অবহেলা রোধ সহ সুনিশ্চিত জীবনযাত্রার মান নিশ্চিত করা সম্ভব বলে মনে করি।\r\nএবং স্থানীয় কাউন্সিলর জবাবদিহিতা সচ্ছতার প্রমান স্বরুপ হোল্ডিং,ফ্লাটের বা স্থানীয় ভোটারের  অভিযোগ প্রদানের জন্য আধুনিক একটি প্লাটফর্ম তৈরী করা এলাকা বা ওয়ার্ড পর্যায়ে একটি নেটওয়ার্কং সার্ভারের মাধ্যমে ডিজিটাল পদ্ধতি ব্যবহার করা যেতে পারে।', '2020-01-26 03:05:58', '2020-01-26 03:05:58'),
(24, 'Tomal Ahsan', '01913475007', 'tomalahsan@gmail.com', '44', 1, 'Research Consultant', '48', 'Flat C1, 36 Zigatola\r\nCivil Chand Villa\r\nDhaka-1209', 2, 6, NULL, 'Honorable Future Mayor,\r\n\r\nWith high hope I am wishing you all the best with your honest endeavors. We have great respect for you and high hopes with the election outcome. May Almighty Allah grant you success in the upcoming election and bestow the opportunities to materialize all your plans.\r\n\r\nI am writing this as a personal request from a citizen with concerns for this beautiful city of ours. We are missing an opportunity to mitigate the traffic situation or at least try to reduce the heavy traffic while entering Zigatola. The alley or bi-road beside Bank Asia, Satmasjid Road, Dhanmondi 2/1 could be extended and added to be used as an entry point (one way)for Zigatola reducing the incoming traffic. There is an office of WAPDA and Government quarters which are still under construction (that could be corrected) .This bi-lane will reduce the stress to 50%. \r\n\r\nAs you are aware that the Zigatola road is among the busiest and used diversely and frequented by at least 5 million commuters everyday. \r\n\r\nOnly you have the capability, farsightedness and dedication to bring about the much needed change. All it needs is connecting the two existing roads to make way for incoming traffic towards Zigatola just before we enter Zigatola.\r\nI am sending the links of these maps/images files so that you could at least consider the options before the construction proceeds further blocking the road once and for all.\r\n\r\nhttps://drive.google.com/file/d/1IIL00qtbjJDbiDTKN4LasglBG2VB-cLl/view?usp=sharing\r\n\r\nhttps://drive.google.com/file/d/18kemBLLyk9o1LJ40fjntmqeX0L8p2IjD/view?usp=sharing\r\n\r\nOnce again, good luck with the election, our prayers and continuous support will always be with you.\r\n\r\nRegards,\r\nTomal', '2020-01-26 04:59:19', '2020-01-26 04:59:19');

-- --------------------------------------------------------

--
-- Table structure for table `opinion_types`
--

CREATE TABLE `opinion_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `opinion_types`
--

INSERT INTO `opinion_types` (`id`, `name`, `sort`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'সাল ভিত্তিক', 0, NULL, NULL, NULL),
(2, 'ঐতিহ্যের ঢাকা', 1, NULL, NULL, NULL),
(3, 'সুন্দর ঢাকা', 2, NULL, NULL, NULL),
(4, 'সচল ঢাকা', 3, NULL, NULL, NULL),
(5, 'সুশাসিত ঢাকা', 4, NULL, NULL, NULL),
(6, 'উন্নত ঢাকা', 5, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `opinion_type_subs`
--

CREATE TABLE `opinion_type_subs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `opinion_type_id` int(10) UNSIGNED NOT NULL COMMENT 'primary kew of opinion_types table',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `opinion_type_subs`
--

INSERT INTO `opinion_type_subs` (`id`, `opinion_type_id`, `name`, `sort`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 'ঐতিহ্য পুনরদ্ধার ও বিকাশ সম্পর্কিত', 0, NULL, NULL, NULL),
(2, 2, 'ঢাকার প্রত্নতত্ত্ব সংরক্ষণ সম্পর্কিত', 1, NULL, NULL, NULL),
(3, 2, 'পর্যটন নগরী ঢাকা সম্পর্কিত', 2, NULL, NULL, NULL),
(4, 2, 'ঐতিহ্যের ঢাকার সুব্যবস্থাপনা সম্পর্কিত', 3, NULL, NULL, NULL),
(5, 2, 'ঢাকার স্বকীয়তাকে বিশ্ব দরবারে প্রস্ফুটিতকরণ সম্পর্কিত', 4, NULL, NULL, NULL),
(6, 2, 'মহাপরিকল্পনায় নাগরিক প্রত্যাশা', 5, NULL, NULL, NULL),
(7, 2, 'অন্যান্য', 6, NULL, NULL, NULL),
(8, 3, 'বুড়িগঙ্গা ও শীতলক্ষ্যার দূষণ হ্রাস সম্পর্কিত', 0, NULL, NULL, NULL),
(9, 3, 'বুড়িগঙ্গা ও শীতলক্ষ্যার পাড় ঘিরে বণায়ন সম্পর্কিত', 1, NULL, NULL, NULL),
(10, 3, 'নগর সবুজায়ন ও সৌন্দর্য বর্ধন সম্পর্কিত', 2, NULL, NULL, NULL),
(11, 3, 'পরিবেশ বান্ধব স্থাপনা নির্মাণ সম্পর্কিত', 3, NULL, NULL, NULL),
(12, 3, 'বর্জ্য ব্যবস্থাপনা সম্পর্কিত', 4, NULL, NULL, NULL),
(13, 3, 'বায়ু দূষণ রোধ সম্পর্কিত', 5, NULL, NULL, NULL),
(14, 3, 'খেলার মাঠ উদ্ধার ও উন্নয়ন সম্পর্কিত', 6, NULL, NULL, NULL),
(15, 3, 'নারী-শিশু ও প্রবীণদের জন্যে হাঁটার উন্মুক্ত স্থান সম্পর্কিত', 7, NULL, NULL, NULL),
(16, 3, 'বিনোদন কেন্দ্র স্থাপন সম্পর্কিত', 8, NULL, NULL, NULL),
(17, 3, 'অন্যান্য', 9, NULL, NULL, NULL),
(18, 4, 'যানজট নিরসনে কার্যকর উদ্যোগ সম্পর্কিত', 0, NULL, NULL, NULL),
(19, 4, 'গণপরিবহন সুব্যবস্থাপনা সম্পর্কিত', 1, NULL, NULL, NULL),
(20, 4, 'নারী নিরাপত্তায় কার্যকর ব্যবস্থা সম্পর্কিত', 2, NULL, NULL, NULL),
(21, 4, 'নদীর পাড়ে সুপ্রশস্থ রাস্তা সম্পর্কিত', 3, NULL, NULL, NULL),
(22, 4, 'পায়ে হেঁটে, সাইকেল, রিক্সা, ঘোড়ার গাড়ি ও দ্রুতগামী যানবাহন চলাচলে আলাদা লেন ব্যবস্থাপনা সম্পর্কিত', 4, NULL, NULL, NULL),
(23, 4, 'নিরাপদ সড়ক ব্যবস্থা নিশ্চিত সম্পর্কিত', 5, NULL, NULL, NULL),
(24, 4, 'অন্যান্য', 6, NULL, NULL, NULL),
(25, 5, 'ডেঙ্গু ও অন্যান্য সংক্রামক রোগ মুক্ত ঢাকা নিশ্চিত সম্পর্কিত', 0, NULL, NULL, NULL),
(26, 5, '৯০ দিনের মধ্যে মৌলিক সেবা নিশ্চিত সম্পর্কিত', 1, NULL, NULL, NULL),
(27, 5, 'মাদক নির্মূল, নৈতিকতা, সুশাসন প্রতিষ্ঠায় পঞ্চায়েত ব্যবস্থা কার্যকর সম্পর্কিত', 2, NULL, NULL, NULL),
(28, 5, 'দুর্নীতি মুক্ত ঢাকা দক্ষিণ সিটি কর্পোরেশন নিশ্চিত সম্পর্কিত', 3, NULL, NULL, NULL),
(29, 5, 'বছরের ৩৬৫ দিন সেবাদান সম্পর্কিত', 4, NULL, NULL, NULL),
(30, 5, 'প্রকল্প সম্পন্ন হওয়ার ৩ বছরের আগে সেবাদানকারী অন্যান্য সংস্থাকে স্থাপনা নষ্ট করতে না দেয়া এবং উন্নয়ন কাজে নির্দিষ্ট সময়ের পূর্বে অনুমোদন গ্রহণ সম্পর্কিত', 6, NULL, NULL, NULL),
(31, 5, 'অন্যান্য', 5, NULL, NULL, NULL),
(32, 6, '২০৪১ সালের উন্নত বাংলাদেশ ও উন্নত রাজধানী নিশ্চিতে উদ্যোগ সম্পর্কিত', 0, NULL, NULL, NULL),
(33, 6, '৩০ বছর মেয়াদী মহা-পরিকল্পনা সম্পর্কিত', 1, NULL, NULL, NULL),
(34, 6, 'টেকসই কাজ ও কাজের মান কমপক্ষে দশ বছর স্থায়ীত্ব সম্পর্কিত', 2, NULL, NULL, NULL),
(35, 6, 'অন্যান্য', 3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin user', 'admin@cloudnextltd.com', NULL, '$2y$10$t2z4ZtO6X6xF3tJuoItzzOKmWX024ffCZ6BtJ.RCKWNT1MELW4vw.', 'qGTz8SP0Zeze30PGcYDXGOHbQ1jVUM3omqp5uj8ewZXPrExAEdkZrehJKdya', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opinions`
--
ALTER TABLE `opinions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opinion_types`
--
ALTER TABLE `opinion_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opinion_type_subs`
--
ALTER TABLE `opinion_type_subs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `opinions`
--
ALTER TABLE `opinions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `opinion_types`
--
ALTER TABLE `opinion_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `opinion_type_subs`
--
ALTER TABLE `opinion_type_subs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
