<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
    // date accessor for date show
    function getCreatedAtAttribute($value){
        return date('d M, Y H:i A', strtotime($value)); 
    }
}
