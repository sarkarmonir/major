<?php
/**
 * for any single value
 * @author MR
 */
function hybrid_first($table,$column,$case,$select) {
    $result = DB::table("$table")
            ->where("$column", $case)
            ->select($select)
            ->first();
    if (isset($result->$select)) {
        return $result->$select;
    } else {
        return null;
    }
}
/**
 * for any single value
 * @author MR
 */
function hybrid_get($table,$column,$case,$select) {
    $result = DB::table("$table")
            ->where("$column", $case)
            ->select($select)
            ->get();
    if (isset($result)) {
        return $result;
    } else {
        return null;
    }
}
