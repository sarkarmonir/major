<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OpinionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'phone' => 'required|max:191',
            'email' => 'nullable|email|max:191',
            'age' => 'max:191',
            'gender' => 'required',
            'profession' => 'max:191',
            'ward' => 'max:191',
            'type' => 'max:191',
            'sub_type' => 'max:191',
            'year' => 'max:191',
            'opinion' => 'required',
        ];
    }

    public function messages()
    {   
        return [
            'name.required' => 'অবশ্যই নাম লিখতে হবে!',
            'name.max' => 'নাম এর অনেক বেশি টেক্সট দিয়েছেন!',
            'phone.required' => 'অবশ্যই মোবাইল নম্বর লিখতে হবে!',
            'phone.max' => 'মোবাইল নম্বর এর অনেক বেশি টেক্সট দিয়েছেন!',
            'email.email' => 'ই-মেইল এর ফরমেট ঠিক নেই!',
            'email.max' => 'ই-মেইল এর অনেক বেশি টেক্সট দিয়েছেন!',
            'age.max' => 'বয়স এর অনেক বেশি টেক্সট দিয়েছেন!',
            'gender.required' => 'অবশ্যই লিঙ্গ নির্বাচন করতে হবে!',
            'profession.max' => 'পেশা এর অনেক বেশি টেক্সট দিয়েছেন!',
            'ward.max' => 'ওয়ার্ড এর অনেক বেশি টেক্সট দিয়েছেন!',
            // 'type.required' => 'অবশ্যই মতামতের ধরণ নির্বাচন করতে হবে!',
            'type.max' => 'ওয়ার্ড এর অনেক বেশি টেক্সট দিয়েছেন!',
            'year.max' => 'সাল এর অনেক বেশি টেক্সট দিয়েছেন!',
            'opinion.required' => 'অবশ্যই মতামত লিখতে হবে!'
        ]; 
    }
}