<?php

namespace App\Http\Controllers;

use App\Http\Requests\OpinionRequest;
use Illuminate\Http\Request;
use App\Opinion;
use App\opinion_types;
use App\OpinionTypeSubs;
use Session;
// use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OpinionController extends Controller
{
    /**
     * for save public opinoin
     */
    public function store(OpinionRequest $request){
        // dd($request);
        try {
            $pinion = new Opinion();
            $pinion->name = $request->name;
            $pinion->phone = $request->phone;
            $pinion->email = $request->email;
            $pinion->age = $request->age;
            $pinion->gender = $request->gender;
            $pinion->profession = $request->profession;
            $pinion->ward = $request->union;
            $pinion->address = $request->address;
            $pinion->type = $request->type;
            // $pinion->sub_type = $request->sub_type;
            $pinion->opinion = $request->opinion;
            $pinion->year = $request->year;
            $pinion->save();
            // show success msg
            // $data['name']=$pinion->name;
            // return redirect()->back();
            // Toastr::success('Messages in here', 'Title');
            Session::flash('name', $pinion->name);
            return redirect('/success');
        } catch (ModelNotFoundException $e) {
            // Toastr::error($e->getMessage(), 'Warning');
            return redirect()->back();
        }
    }
    /**
     * for show success page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function success()
    {
        if(!Session::has('name')){
            return redirect('/');
        }
        $data['title'] = 'শেখ ফজলে নূর তাপস';
        $data['is_success'] = true;
        return view('success', $data);
    }
    /**
     * get sub type of main type
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function get_subtype(Request $request)
    {
        $result = OpinionTypeSubs::where('opinion_type_id', $request->type)->orderBy('sort','asc')->get();
        return response()->json($result, 200);
    }
}