<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Note;
use App\Opinion;
use App\opinion_types;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class OpinionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * for show opinoin list
     */
    public function index(Request $request)
    {
        $data['request'] = $request;
        $data['title'] = 'Opinion List';
        $query = Opinion::leftJoin('opinion_types','opinions.type','=','opinion_types.id');
                        // ->leftJoin('opinion_type_subs','opinions.sub_type','=','opinion_type_subs.id');
        if($request->name){
            $query->where('opinions.name', 'like', '%' . $request->name . '%');
        }
        if($request->phone){
            $query->where('opinions.phone', 'like', '%' . $request->phone . '%');
        }
        if($request->email){
            $query->where('opinions.email', 'like', '%' . $request->email . '%');
        }
        if($request->age){
            $query->where('opinions.age', 'like', '%' . $request->age . '%');
        }
        if($request->profession){
            $query->where('opinions.profession', 'like', '%' . $request->profession . '%');
        }
        if($request->union){
            $query->where('opinions.ward', 'like', '%' . $request->union . '%');
        }
        if($request->address){
            $query->where('opinions.address', 'like', '%' . $request->address . '%');
        }
        if($request->type){
            $query->where('opinions.type',$request->type);
        }
        // if($request->sub_type){
        //     $query->where('opinions.sub_type',$request->sub_type);
        // }
        if($request->year){
            $query->where('opinions.year',$request->year);
        }
        $data['lists'] = $query->select('opinions.*','opinion_types.name as opinion_type_name')->orderBy('id','desc')->paginate(20); //,'opinion_type_subs.name as opinion_sub_type_name'
        $data['types'] = opinion_types::orderBy('sort','asc')->get();
        return view('backend.opinion.list', $data);
    }

    /*
     */
    public function create(Request $request)
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
    /**
     * add note
     */
    public function note_add(Request $request)
    {
        try {
            $data = new Note();
            $data->opinion_id = $request->id;
            $data->note = $request->note;
            $data->save();
            Toastr::success('যুক্ত হয়েছে', 'সফল');
            return redirect()->back();
        } catch (ModelNotFoundException $e) {
            Toastr::error($e->getMessage(), 'Warning');
            return redirect()->back();
        }
    }

}
