<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Opinion;
use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * opinion PDF generate
     */
    public function opinion_pdf(Request $request)
    {
        // dd($request);
        $data['lists'] = Opinion::leftJoin('opinion_types','opinions.type','=','opinion_types.id')
                                    ->where('opinions.name', 'like', '%' . $request->name . '%')
                                    ->where('opinions.phone', 'like', '%' . $request->phone . '%')
                                    ->where('opinions.email', 'like', '%' . $request->email . '%')
                                    ->where('opinions.age', 'like', '%' . $request->age . '%')
                                    ->where('opinions.profession', 'like', '%' . $request->profession . '%')
                                    ->where('opinions.ward', 'like', '%' . $request->ward . '%')
                                    ->where('opinions.address', 'like', '%' . $request->address . '%')
                                    ->where('opinion_types.name', 'like', '%' . $request->type . '%')
                                    ->select('opinions.*','opinion_types.name as opinion_type_name')
                                    ->paginate(20);
        $pdf = PDF::loadView('backend.opinion.opinion_pdf', $data);
        // $pdf->setPaper('A4', 'landscape');
        // $pdf->SetProtection(['copy', 'print'], '', 'pass');
        return $pdf->stream('pdfview.pdf');

        // $pdf = PDF::loadView('pdf.document', $data);
        // $pdf->SetProtection(['copy', 'print'], '', 'pass');
        // return $pdf->stream('document.pdf');
    }
}
