<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\opinion_types;
use Session;

class HomeController extends Controller
{

    /**
     * Show the application home.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['title'] = 'শেখ ফজলে নূর তাপস';
        $data['is_success'] = false;
        $data['types'] = opinion_types::orderBy('sort','asc')->get();
        Session::flash('success', 'Successfully Added!');
        // dd($data);
        return view('home', $data);
    }
    /**
     * Show the application commitment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function commitment()
    {
        $data['title'] = 'শেখ ফজলে নূর তাপস';
        $data['is_success'] = false;
        $data['types'] = opinion_types::orderBy('sort','asc')->get();
        return view('commitment', $data);
    }
}
