<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpinionTypeSubs extends Model
{
    // use soft delete
    use SoftDeletes;
}
