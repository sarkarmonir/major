<!DOCTYPE html>
<html>
    <head>
        <title>User list - PDF</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <style>
        body{
            font-family:bangla;
        }
        .table-pdf {
            width: 100%;
        }
        .table-pdf thead tr th{
            text-align: center;
            background: #eee;
            font-weight: bold;
            padding: 5px 10px;
            font-size: 17px;
        }
        .table-pdf thead tr th, .table-pdf tbody tr td {
            padding: 4px;
            font-size: 12px !important;
        }
        table.table tbody tr td{
            background: transparent !important;
            vertical-align: top;
        }
        table.table-header{
            width: 100%;
        }
        table.table-header tr td.last-child{
            text-align: right !important;
        }
        .content-header h2, .content-header h3{
            margin: 0px;
            padding: 0px;
        }
        .bg-wm{
            background: url("assets/back/dist/images/gov-logo-wm.png");
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;
        }
        </style>
    </head>
    <body>
        <style>
            body {
                font-family: 'bangla', sans-serif;
            }
        </style>
        <div class="container">
            <div class="row">
                <div class="box box-success profile-sec bn">
                    <h3 class="text-center m-0 p-0">প্রত্যাশিত ঢাকার (দক্ষিণ) নবসূচনায় নাগরিক মতামত</h3>
                    <p class="text-center m-0 p-0">রিপোর্টিং টাইম: {{ date('d M, Y h:i A')  }}</p>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- header -->
                            <div class="row col-md-12">
                                <table id="" class="table table-responsive table-bordered table-report bg-logo table-pdf">
                                    <thead>
                                        <tr>
                                            <th>নং</th>
                                            <th>নাম</th>
                                            <th>মোবাইল</th>
                                            <th>বয়স</th>
                                            <th>লিঙ্গ</th>
                                            <th>পেশা</th>
                                            <th>ওয়ার্ড</th>
                                            <th>ঠিকানা</th>
                                            <th>মতামতের ধরণ</th>
                                            <th>তারিখ ও সময়</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($lists as $key => $list)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $list->name }}</td>
                                            <td>{{ $list->phone }}</td>
                                            <td>{{ $list->age }}</td>
                                            <td>{{ $list->gender }}</td>
                                            <td>{{ $list->profession }}</td>
                                            <td>{{ $list->ward }}</td>
                                            <td>{{ $list->address }}</td>
                                            <td>{{ $list->opinion_type_name }}</td>
                                            <td>{{ $list->created_at }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>