@extends('auth.layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3 class="text-center">প্রত্যাশিত দাউদকান্দি উপজেলা নবসূচনায় নাগরিক মতামত</h3>
            <br>
            <form method="GET" id="search_form">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label class="sr-only" for="inlineFormInput">নাম</label>
                        <input type="text" class="form-control mb-2" id="inlineFormInput" name="name" value="{{ $request->name }}" placeholder="নাম">
                    </div>
                    <div class="col-auto">
                        <label class="sr-only" for="inlineFormInput">মোবাইল</label>
                        <input type="text" class="form-control mb-2" id="inlineFormInput" name="phone" value="{{ $request->phone }}" placeholder="মোবাইল">
                    </div>
                    <div class="col-auto">
                        <label class="sr-only" for="inlineFormInput">ই-মেইল</label>
                        <input type="text" class="form-control mb-2" id="inlineFormInput" name="email" value="{{ $request->email }}" placeholder="ই-মেইল">
                    </div>
                    <div class="col-auto">
                        <label class="sr-only" for="inlineFormInput">বয়স</label>
                        <input type="text" class="form-control mb-2" id="inlineFormInput" name="age" value="{{ $request->age }}" placeholder="বয়স">
                    </div>
                    <div class="col-auto">
                        <label class="sr-only" for="inlineFormInput">পেশা</label>
                        <input type="text" class="form-control mb-2" id="inlineFormInput" name="profession" value="{{ $request->profession }}" placeholder="পেশা">
                    </div>
                    <div class="col-auto">
                        <label class="sr-only" for="inlineFormInput">ইউনিয়ন</label>

                        <!-- <input type="text" class="form-control mb-2" id="inlineFormInput" name="ward" value="{{ $request->ward }}" placeholder="ইউনিয়ন"> -->
                        <select  class="form-control" name="union" id="union">
                            <option value="3" {{ $request->ward == 3?'selected':''}}>দাউদকান্দি পৌরসভা</option>
                            <option value="2" {{ $request->ward == 2?'selected':''}}>দাউদকান্দি (উত্তর)</option>
                            <option value="6" {{ $request->ward == 6?'selected':''}}>সুন্দলপুর</option>
                            <option value="14" {{ $request->ward == 14?'selected':''}}>বারপাড়া</option>
                            <option value="7" {{ $request->ward == 7?'selected':''}}>গৌরীপুর</option>
                            <option value="5" {{ $request->ward == 5?'selected':''}}>জিংলাতলী</option>
                            <option value="4" {{ $request->ward == 4?'selected':''}}>ইলিয়টগঞ্জ (দক্ষিন)</option>
                            <option value="15" {{ $request->ward == 15?'selected':''}}>ইলিয়টগঞ্জ (উত্তর)</option>
                            <option value="11" {{ $request->ward == 11?'selected':''}}>বিটেশ্বর</option>
                            <option value="10" {{ $request->ward == 10?'selected':''}}>মারুকা</option>
                            <option value="1" {{ $request->ward == 1?'selected':''}}>দৌলতপুর</option>
                            <option value="8" {{ $request->ward == 8?'selected':''}}>মোহাম্মদপুর (পশ্চিম)</option>
                            <option value="16" {{ $request->ward == 16?'selected':''}}>মালিগাঁও</option>
                            <option value="12" {{ $request->ward == 12?'selected':''}}>পদুয়া</option>
                            <option value="9" {{ $request->ward == 9?'selected':''}}>গোয়ালমারী</option>
                            <option value="13" {{ $request->ward == 13?'selected':''}}>পাচঁগাছিয়া (পশ্চিম)</option>
                        </select>
                    </div>
                    <div class="col-auto">
                        <label class="sr-only" for="inlineFormInput">ঠিকানা</label>
                        <input type="text" class="form-control mb-2" id="inlineFormInput" name="address" value="{{ $request->address}}" placeholder="ঠিকানা">
                    </div>
                    <div class="col-auto">
                        <label class="sr-only" for="inlineFormInput">মতামতের ধরণ</label>
                        <select class="form-control mb-2" id="inlineFormInput" name="type">
                            <option value="">ধরণ নির্বাচন</option>
                            @foreach($types as $type)
                            <option value="{{$type->id}}" {{ ($request->type == $type->id)?'selected':'' }}>{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-auto">
                        <label class="sr-only" for="inlineFormInput">সাল ভিত্তিক</label>
                        <select class="form-control mb-2" id="inlineFormInput" name="year">
                            <option value="">সাল নির্বাচন</option>
                            <option value="90" {{ ($request->year == '90')?'selected':'' }}>৯০ তম দিন</option>
                            <option value="2020" {{ ($request->year == '2020')?'selected':'' }}>২০২০</option>
                            <option value="2021" {{ ($request->year == '2021')?'selected':'' }}>২০২১</option>
                            <option value="2022" {{ ($request->year == '2022')?'selected':'' }}>২০২২</option>
                            <option value="2023" {{ ($request->year == '2023')?'selected':'' }}>২০২৩</option>
                            <option value="2024" {{ ($request->year == '2024')?'selected':'' }}>২০২৪</option>
                            <option value="2025" {{ ($request->year == '2025')?'selected':'' }}>২০২৫</option>
                            <option value="2030" {{ ($request->year == '2030')?'selected':'' }}>২০৩০</option>
                            <option value="2035" {{ ($request->year == '2035')?'selected':'' }}>২০৩৫</option>
                            <option value="2041" {{ ($request->year == '2041')?'selected':'' }}>২০৪১</option>
                            <option value="2050" {{ ($request->year == '2050')?'selected':'' }}>২০৫০</option>
                    </select>
                    </div>
                    <div class="col-auto">
                        <button type="submit" class="btn btn-info mb-2">সার্চ করুন</button>
                    </div>
                    <div class="col-auto">
                        <button type="button" onclick="document.getElementById('search_form').setAttribute('target', '_blank'); document.getElementById('search_form').action = 'opinion-pdf'; document.getElementById('search_form').submit();" class="btn btn-success mb-2" >প্রিন্ট করুন</button>
                    </div>
                </div>
            </form>
            <br>
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th scope="col" width="5%">#</th>
                    <th scope="col" width="15%">নাম</th>
                    <th scope="col" width="15%">মোবাইল</th>
                    <th scope="col" width="10%">বয়স</th>
                    <th scope="col" width="10%">লিঙ্গ</th>
                    <th scope="col" width="10%">পেশা</th>
                    <th scope="col" width="10%">ইউনিয়ন</th>
                    <th scope="col" width="15%">ঠিকানা</th>
                    <th scope="col" width="10%">মূল ধরণ</th>
                    <!-- <th scope="col" width="10%">সাব ধরণ</th> -->
                    <th scope="col" width="10%">তারিখ ও সময়</th>
                    <th scope="col" width="10%">অ্যাকশন</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($lists as $key => $list)
                    <tr>
                        <th scope="row">{{ ++$key }}</th>
                        <td>{{ $list->name }}</td>
                        <td>{{ $list->phone }}</td>
                        <td>{{ $list->age }}</td>
                        <td>
                            @if($list->gender == 1)
                            <h5><span class="badge badge-primary">পুরুষ</span></h5>
                            @elseif($list->gender == 2)
                            <h5><span class="badge badge-info">নারী</span></h5>
                            @elseif($list->gender == 3)
                            <h5><span class="badge badge-warning">অন্যান্য</span></h5>
                            @endif
                        </td>
                        <td>{{ $list->profession }}</td>
                        <td>
                            @if($list->ward == 1) দৌলতপুর @endif
                            @if($list->ward == 2) দাউদকান্দি (উত্তর) @endif
                            <!-- @if($list->ward == 3) দাউদকান্দি (উত্তর) @endif -->
                            @if($list->ward == 4) ইলিয়টগঞ্জ (দক্ষিন) @endif
                            @if($list->ward == 15) ইলিয়টগঞ্জ (উত্তর) @endif
                            @if($list->ward == 5) জিংলাতলী @endif
                            @if($list->ward == 6) সুন্দলপুর @endif
                            @if($list->ward == 7) গৌরীপুর @endif
                            @if($list->ward == 8) মোহাম্মদপুর (পশ্চিম) @endif
                            @if($list->ward == 16) মোহাম্মদপুর (পূর্ব) @endif
                            @if($list->ward == 9) গোয়ালমারী @endif
                            @if($list->ward == 10) মারুকা @endif
                            @if($list->ward == 11) বিটেশ্বর @endif
                            @if($list->ward == 12) পদুয়া @endif
                            @if($list->ward == 13) পাচঁগাছিয়া (পশ্চিম) @endif
                            @if($list->ward == 14) বারপাড়া @endif
                        </td>
                        <td>{{ $list->address }}</td>
                        <td>
                            <!-- ($list->type == 1)?$list->year: -->
                            <h5><span class="badge badge-primary">{{ $list->opinion_type_name }}</span></h5>
                        </td>
                        <!-- <td>
                            <h5><span class="badge badge-secondary">{{ ($list->type == 1)?'':$list->opinion_sub_type_name }}</span></h5>
                        </td> -->
                        <td>{{ $list->created_at }}</td>
                        <td>
                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#viewModal_{{$key}}">দেখুন</button>
                            <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#note_{{$key}}">নোট</button>
                            <!-- note -->
                            <div class="modal fade" id="note_{{$key}}" tabindex="-1" role="dialog" aria-labelledby="noteModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <form action="{{ url('opinion-note-add') }}" method="post">
                                        @csrf
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">নোট</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            @if(!$note[$key] = hybrid_first('notes','opinion_id',$list->id,'note'))
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="note" placeholder="এখানে নোট লিখুন" required></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="id" value="{{ $list->id }}">
                                                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">কেটে দিন</button>
                                                <button type="submit" class="btn btn-primary">সেভ করুন</button>
                                            </div>
                                            @else
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                {{$note[$key]}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">কেটে দিন</button>
                                            </div>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- view Modal -->
                            <div class="modal fade" id="viewModal_{{$key}}" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">বিস্তারিত তথ্য</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <table class="">
                                                <tr>
                                                    <td width="20%">নাম</td>
                                                    <td>{{ $list->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>মোবাইল নং</td>
                                                    <td>{{ $list->phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>ই-মেইল</td>
                                                    <td>{{ $list->email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>লিঙ্গ</td>
                                                    <td>
                                                        @if($list->gender == 1)
                                                        <h5><span class="badge badge-primary">পুরুষ</span></h5>
                                                        @elseif($list->gender == 2)
                                                        <h5><span class="badge badge-info">নারী</span></h5>
                                                        @elseif($list->gender == 3)
                                                        <h5><span class="badge badge-warning">অন্যান্য</span></h5>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>বয়স</td>
                                                    <td>{{ $list->age }}</td>
                                                </tr>
                                                <tr>
                                                    <td>পেশা</td>
                                                    <td>{{ $list->profession }}</td>
                                                </tr>
                                                <tr>
                                                    <td>ওয়ার্ড</td>
                                                    <td>{{ $list->ward }}</td>
                                                </tr>
                                                <tr>
                                                    <td>ঠিকানা</td>
                                                    <td>{{ $list->address }}</td>
                                                </tr>
                                                <tr>
                                                    <td>মতামতের ধরণ</td>
                                                    <td>{{ $list->opinion_type_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>মতামতের ধরণ</td>
                                                    <td>{{ $list->year }}</td>
                                                </tr>
                                                <tr>
                                                    <td>মতামত</td>
                                                    <td>{{ $list->opinion }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">কেটে দিন</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $lists->appends(Request::input())->links() }}
        </div>
    </div>
</div>
@endsection