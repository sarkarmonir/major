@extends('layouts.app')

@section('content')
<section class="vision">
    <div class="container-fluid">
        <div class="vision-sec wow zoomIn">
            <div class="col-md-12 text-center my-5">
                <h3>আমার প্রতিজ্ঞা</h3>
                <p>অর্থনৈতিকভাবে অগ্রসরমান বাংলাদেশের জন্যে একটি আধুনিক রাজধানী গড়বার সময় হয়েছে। সময় হয়েছে ঘুরে দাঁড়াবার- নতুন পথে যাত্রা শুরু করার। ঢাকা উন্নয়নের জন্য এখন দরকার অভিজ্ঞতা সম্পন্ন, বলিষ্ঠ ও সঠিক নেতৃত্ব। অনেক সময় পেরিয়ে গেছে, অনেক অবহেলা আর গাফিলতিতে ঢাকা একটি অপরিকল্পিত নগরীতে পরিণত হয়ে গেছে। এটি এখন দূষণে আক্রান্ত নগরী। আর অবহেলা নয়, আর দেরি নয়। এখনই ঢাকাকে পুনরুজ্জীবিত করতে হবে। আপনাদের সমর্থন ও ভোটে মেয়র নির্বাচিত হলে পাঁচটি পরিকল্পনা নিয়ে অগ্রসর হতে আমি দৃঢ় প্রতিজ্ঞ।</p>
                {{-- <p>২০৪১ সালে বাংলাদেশকে একটি উন্নত দেশে পরিণত করতে হলে দেশের রাজধানী ঢাকাকেই উন্নত এবং যোগ্য করে গড়ে তুলতে আমার অঙ্গীকার</p> --}}
            </div>
            <div class="row align-items-center sec sec-01">
                <div class="col-md-3">
                    <img src="https://media.worldnomads.com/travel-safety/bangladesh/dhaka-at-night-gettyimages-519856065.jpg" alt="image" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h4>ঐতিহ্যের ঢাকা:</h4>
                    <blockquote class="blockquote">
                        <p class="mb-0 text-justify">"চারশত বছরের পুরনো আমাদের এই ঢাকার রয়েছে নিজস্ব ইতিহাসের উজ্জ্বল ছবি ও ঐতিহ্যের গভীর শেকড়। ঢাকার রয়েছে প্রত্নতাত্ত্বিক গুরুত্ব। পর্যটনের জন্যও ঢাকা হতে পারে অপার সম্ভাবনার ক্ষেত্র। এখানে ঐতিহ্যবাহী খাবারের স্বাদও অনন্য। সাংস্কৃতিক ধারায় রয়েছে ঈদ, পহেলা বৈশাখ, ঘুড়ি উৎসব, চৈত্র সংক্রান্তিসহ  অজস্র উৎসব। আমি র্নিবাচিত হলে অভিবাবকত্বের দায়বদ্ধতা থেকে সকলকে নিয়ে সমন্বিত প্রয়াসে সুব্যবস্থাপনা ও মহাপরিকল্পনার মাধ্যমে পুরনো ঢাকার ঐতিহ্যকে পুনরদ্ধার ও সংরক্ষণ করে ঢাকাকে তার নিজস্ব গৌরবে সাজিয়ে তুলে ধরতে চাই বিশ্ব দরবারে।"</p>
                        <div class="blockquote-footer">শেখ ফজলে নূর তাপস, <small>ব্যারিস্টার-এট-ল</small> <br>
                            <span class="ml-4">মেয়র প্রার্থী, ঢাকা দক্ষিণ সিটি কর্পোরেশন</span>
                        </div>
                    </blockquote>
                </div>
                <div class="col-md-3 text-left">
                    <h3>আপনার মতামত দিন</h3>
                    <a class="btn btn-primary btn-outline" data-toggle="modal" data-target="#typeDefineForm" onclick="setType(1)">অনলাইন বার্তা পাঠান</a>
                    <p>অথবা কল করুন 12346579 (১০-৬)</p>
                </div>
            </div>
            <div class="row align-items-center sec sec-02">
                <div class="col-md-3">
                    <img src="https://media.worldnomads.com/travel-safety/bangladesh/dhaka-at-night-gettyimages-519856065.jpg" alt="image" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h4>সুন্দর ঢাকা:</h4>
                    <blockquote class="blockquote">
                        <p class="mb-2 text-justify">"দুটি নদী বুড়িগঙ্গা ও শীতলক্ষ্যা’র আববাহিকায় পত্তন হওয়া এমন শহর পৃথিবীতে বিরল। সুন্দর ঢাকা গড়ে তুলতে সবুজায়ন, পরিবেশ বান্ধব স্থাপনা বৃদ্ধি ও বায়ু দূষণ রোধ করতে চাই। বর্জ্য ব্যবস্থাপনা উন্নয়ন, প্রতিটি ওয়ার্ডে খেলার মাঠ উদ্ধার ও সৃষ্টি করা, নারী-শিশু ও প্রবীনদের জন্যে হাঁটার উন্মুক্ত স্থান, র্দীঘ মেয়াদী পরিকল্পনার আওতায় বুড়িগঙ্গা ও শীতলক্ষ্যার পাড় ঘিরে বণায়ন, বিনোদন কেন্দ্র স্থাপন, নদী দূষণ কমিয়ে এনে সুন্দর ঢাকা প্রতিষ্ঠা করা।"</p>
                        <div class="blockquote-footer">শেখ ফজলে নূর তাপস, <small>ব্যারিস্টার-এট-ল</small> <br>
                            <span class="ml-4">মেয়র প্রার্থী, ঢাকা দক্ষিণ সিটি কর্পোরেশন</span>
                        </div>
                    </blockquote>
                </div>
                <div class="col-md-3 text-left">
                    <h3>আপনার মতামত দিন</h3>
                    <a class="btn btn-primary btn-outline" data-toggle="modal" data-target="#typeDefineForm" onclick="setType(2)">অনলাইন বার্তা পাঠান</a>
                    <p>অথবা কল করুন 12346579 (১০-৬)</p>
                </div>
            </div>
            <div class="row align-items-center sec sec-03">
                <div class="col-md-3">
                    <img src="https://media.worldnomads.com/travel-safety/bangladesh/dhaka-at-night-gettyimages-519856065.jpg" alt="image" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h4>সচল ঢাকা:</h4>
                    <blockquote class="blockquote">
                        <p class="mb-2 text-justify">"যানজটের কারনে আজ নাভিশ্বাস উঠে যায়। সকালে বাসা থেকে বেরিয়ে গন্তব্যে পৌঁছানো ও ঘরে ফিরে আসা যেন সংগ্রাম, বিশেষ করে কর্মজীবি নারীদের বিড়ম্বনা অপরিসীম। অচিরেই মেট্রোরেল চালু হবে কিন্তু শুধু এর দ্বারা পুরোপুরি যানজট মুক্ত হবে না। গণপরিবহনের সুব্যবস্থাপনার মাধ্যমে কিছু রাস্তায় দ্রুতগতির গাড়ি, কিছু রাস্তায় রিক্সা, আবার কোন রাস্তায় শুধু মানুষ হেঁটে চলবে। নদীর পাড়ে থাকবে সুপ্রশস্থ রাস্তা, যেখানে পায়ে হেঁটে চলা যাবে, চালানো যাবে সাইকেল, রিক্সা ও ঘোড়ার গাড়ি, দ্রুতগামী যানবাহনের জন্য থাকবে আলাদা লেন, হবে নিরাপদ সড়ক ব্যবস্থা। দীর্ঘ মেয়াদি সমন্বিত পরিকল্পনায় আমরা এভাবে গড়ে তুলবো আমাদের সচল ঢাকা। "</p>
                        <div class="blockquote-footer">শেখ ফজলে নূর তাপস, <small>ব্যারিস্টার-এট-ল</small> <br>
                            <span class="ml-4">মেয়র প্রার্থী, ঢাকা দক্ষিণ সিটি কর্পোরেশন</span>
                        </div>
                    </blockquote>
                </div>
                <div class="col-md-3 text-left">
                    <h3>আপনার মতামত দিন</h3>
                    <a class="btn btn-primary btn-outline" data-toggle="modal" data-target="#typeDefineForm" onclick="setType(3)">অনলাইন বার্তা পাঠান</a>
                    <p>অথবা কল করুন 12346579 (১০-৬)</p>
                </div>
            </div>
            <div class="row align-items-center sec sec-04">
                <div class="col-md-3">
                    <img src="https://media.worldnomads.com/travel-safety/bangladesh/dhaka-at-night-gettyimages-519856065.jpg" alt="image" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h4>সুশাসিত ঢাকা:</h4>
                    <blockquote class="blockquote">
                        <p class="mb-2 text-justify">"ঐতিহ্যগতভাবে এই ঢাকায় পঞ্চায়েত ব্যবস্থা কার্যকর ছিল। মাদক নির্মূল, নৈতিক অবক্ষয় জনিত অপরাধ রোধ সহ এলাকাভিত্তিক সুশাসন প্রতিষ্ঠায় পঞ্চায়েত ব্যবস্থা পুনঃপ্রবর্তন। সিটি কর্পোরেশনের অভিভাবকত্ব নিয়ে সততা ও নিষ্ঠার সাথে একাগ্রচিত্তে ঢাকাকে নিয়ে কাজ করতে চাই। বাংলাদেশে স্বায়ত্বশাসিত প্রতিষ্ঠান হিসেবে ঢাকা দক্ষিণ সিটি কর্পোরেশন হবে প্রথম দুর্নীতি মুক্ত সংস্থা। আইন ও রীতি নীতির বাস্তব প্রয়োগ হবে। নগর ভবন বছরের ৩৬৫ দিন, মাসের ৩০ দিন, সপ্তাহের ৭ দিন ও ২৪ ঘন্টা নাগরিক সেবা প্রদানের জন্য খোলা থাকবে। ঢাকার উন্নয়ন ও সেবার সাথে সংশ্লিষ্ট অন্যান্য সংস্থাকে সিটি কর্পোরেশনের নিকট সমন্বিতভাবে দায়বদ্ধ করা হবে।"</p>
                        <div class="blockquote-footer">শেখ ফজলে নূর তাপস, <small>ব্যারিস্টার-এট-ল</small> <br>
                            <span class="ml-4">মেয়র প্রার্থী, ঢাকা দক্ষিণ সিটি কর্পোরেশন</span>
                        </div>
                    </blockquote>
                </div>
                <div class="col-md-3 text-left">
                    <h3>আপনার মতামত দিন</h3>
                    <a class="btn btn-primary btn-outline" data-toggle="modal" data-target="#typeDefineForm" onclick="setType(4)">অনলাইন বার্তা পাঠান</a>
                    <p>অথবা কল করুন 12346579 (১০-৬)</p>
                </div>
            </div>
            <div class="row align-items-center sec sec-05">
                <div class="col-md-3">
                    <img src="https://media.worldnomads.com/travel-safety/bangladesh/dhaka-at-night-gettyimages-519856065.jpg" alt="image" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h4>উন্নত ঢাকা:</h4>
                    <blockquote class="blockquote">
                        <p class="mb-2 text-justify">"মাননীয় প্রধানমন্ত্রী বঙ্গবন্ধু কন্যা জননেত্রী শেখ হাসিনা ২০৪১ সালে সুখী-সমৃদ্ধ উন্নত বাংলাদেশ গড়ে তোলার যে কর্মসূচী নিয়েছেন তার পূর্ণ বাস্তবায়ন করতে হলে উন্নত রাজধানী ও উন্নত ঢাকা গড়ে তোলার বিকল্প নাই। অবহেলা, অভিবাবকত্বহীনতা, নিষ্ঠা ও সততার অভাবে অনেক সময় পেরিয়ে গেছে; কিন্তু সময় এখনো শেষ হয়ে যায় নি।  দীর্ঘ ত্রিশ বছর মেয়াদী মহা-পরিকল্পনা প্রণয়ন ও প্রত্যেকটি উন্নয়নমূলক কার্যক্রমের মান নিরুপণ করে দশ বছর স্থায়িত্ব নিশ্চিত করা হবে। সততা, নিষ্ঠা, দক্ষতা ও দায়িত্বশীলতার সাথে কাজ করে আমাদের ঢাকা-ঐতিহ্যের ঢাকা- উন্নত ঢাকা-গড়ে তুলবো ইনশাআল্লাহ্।"</p>
                        <div class="blockquote-footer">শেখ ফজলে নূর তাপস, <small>ব্যারিস্টার-এট-ল</small> <br>
                            <span class="ml-4">মেয়র প্রার্থী, ঢাকা দক্ষিণ সিটি কর্পোরেশন</span>
                        </div>
                    </blockquote>
                </div>
                <div class="col-md-3 text-left">
                    <h3>আপনার মতামত দিন</h3>
                    <a class="btn btn-primary btn-outline" data-toggle="modal" data-target="#typeDefineForm" onclick="setType(5)">অনলাইন বার্তা পাঠান</a>
                    <p>অথবা কল করুন 12346579 (১০-৬)</p>
                </div>
            </div>
            <div class="row justify-content-center my-4">
                <div class="col-md-3">
                    <img src="https://media.worldnomads.com/travel-safety/bangladesh/dhaka-at-night-gettyimages-519856065.jpg" alt="image" class="img-fluid">
                    <div class="text-center">
                        <h3>ডেঙ্গু সম্পর্কিত</h3>
                        <a class="btn btn-primary btn-outline" data-toggle="modal" data-target="#typeDefineForm" onclick="setType(6)">অনলাইন বার্তা পাঠান</a>
                        <p>অথবা কল করুন 12346579 (১০-৬)</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <img src="https://media.worldnomads.com/travel-safety/bangladesh/dhaka-at-night-gettyimages-519856065.jpg" alt="image" class="img-fluid">
                    <div class="text-center">
                        <h3>বুড়িগঙ্গা ও শীতলক্ষ্যা দূষণ</h3>
                        <a class="btn btn-primary btn-outline" data-toggle="modal" data-target="#typeDefineForm" onclick="setType(7)">অনলাইন বার্তা পাঠান</a>
                        <p>অথবা কল করুন 12346579 (১০-৬)</p>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
@include('include.common_form')
@include('include.type_define_form')
@endsection

