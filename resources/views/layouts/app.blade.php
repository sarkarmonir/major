<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>মেজর মোহাম্মদ আলী (অব:)</title>
    <meta name="keywords" content="আমাদের দাউদকান্দি, আমাদের ঐতিহ্য - মেজর মোহাম্মদ আলী (অব:)">
    <meta name="description" content="প্রত্যাশিত দাউদকান্দি উপজেলার নবসূচনায় - মেজর মোহাম্মদ আলী (অব:), ঐতিহ্য, ইতিহাস, দাউদকান্দির প্রত্নতত্ত্ব, পর্যটন, খাবার, উৎসব, সুব্যবস্থাপনা, মহাপরিকল্পনা, ঐতিহ্য পুনরদ্ধার, সংরক্ষণ ও বিকাশ, বুড়িগঙ্গা, শীতলক্ষ্যা, সুন্দর দাউদকান্দি, নগর সবুজায়ন, পরিবেশ বান্ধব স্থাপনা, বায়ু দূষণ রোধ, বর্জ্য ব্যবস্থাপনা, খেলার মাঠ উদ্ধার, হাঁটার স্থান, র্দীঘ মেয়াদী পরিকল্পনার, বুড়িগঙ্গা ও শীতলক্ষ্যার পাড় ঘিরে বণায়ন, বিনোদন কেন্দ্র স্থাপন, নদী দূষণ রোধ, সচল দাউদকান্দি, যানজট, কর্মজীবি নারী, গণপরিবহন সুব্যবস্থাপনা, নদীর পাড়, সুপ্রশস্থ রাস্তা, পায়ে হেঁটে চলাচল, সাইকেল, রিক্সা, ঘোড়ার গাড়ি, দ্রুতগামী যানবাহন, আলাদা লেন, নিরাপদ সড়ক ব্যবস্থা, দীর্ঘ মেয়াদি সমন্বিত পরিকল্পনা, ঐতিহ্য, পঞ্চায়েত ব্যবস্থা, মাদক নির্মূল, নৈতিকতা, সুশাসন প্রতিষ্ঠা, অভিভাবকত্ব, সততা, নিষ্ঠা, দুর্নীতি মুক্ত দাউদকান্দি দক্ষিণ সিটি কর্পোরেশন, আইন ও রীতি নীতির বাস্তব প্রয়োগ, ৩৬৫ দিন সেবা, সুশাসিত দাউদকান্দি, দায়বদ্ধতা, প্রধানমন্ত্রী, বঙ্গবন্ধু কন্যা, জননেত্রী, শেখ হাসিনা, ২০৪১, উন্নত বাংলাদেশ, উন্নত রাজধানী,  উন্নত দাউদকান্দি, নিষ্ঠা, সততা, ত্রিশ বছর মেয়াদী মহা-পরিকল্পনা, কাজের মান, টেকসই কার্যক্রম, দশ বছর স্থায়ীত্ব, দায়িত্বশীল, আমাদের দাউদকান্দি, ঐতিহ্যের দাউদকান্দি, উন্নত দাউদকান্দি">
    <meta property="og:url" content="http://amaderdhaka.com.bd/">
    <meta property="og:type" content="website">
    <meta property="og:title" content="আমাদের দাউদকান্দি, আমাদের ঐতিহ্য - মেজর মোহাম্মদ আলী (অব:)">
    <meta property="og:image" content="{{ asset('front/images/profile/taposh-sqr.png') }}">
    <meta property="og:description" content="প্রত্যাশিত দাউদকান্দি উপজেলার নবসূচনায় - মেজর মোহাম্মদ আলী (অব:)">
    <link rel="shortcut icon" href="{{ asset('front/images/logo/dscc-logo.png') }} " type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('front/css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front/vendors/css/bootstrap.min.css') }}">
    <!-- new plugins -->
    <link href="{{ asset('front/vendors/timeline/zoomtimeline.css') }}" rel="stylesheet">
    <script src="{{ asset('front/vendors/timeline/jquery.js') }}" type="text/javascript"></script>
    <link href="{{ asset('front/vendors/timeline/preseter.css') }}" rel="stylesheet">
    <script src="{{ asset('front/vendors/timeline/preseter.js') }}" type="text/javascript"></script>
    <link href="{{ asset('front/vendors/timeline/farbtastic.css') }}" rel="stylesheet">
    <script src="{{ asset('front/vendors/timeline/farbtastic.js') }}" type="text/javascript"></script>
    <link rel='stylesheet' type="text/css" href="{{ asset('front/vendors/timeline/style.css') }}"/>
    <!-- / new plugins -->
    <link rel="stylesheet" href="{{ asset('front/vendors/css/animate.min.css') }}">
    <!-- lightgallery -->
    <link rel="stylesheet" href="{{ asset('front/vendors/lightbox/dist/css/lightgallery.min.css') }}">
    <!-- fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- <script defer src="js/fa.js"></script> -->
    <script>var BASE_URL = '{{ url('/') }}';</script>
    <style>
        .btn-float {
            position: absolute;
            bottom: 20%;
            left: 50%;
            transform: translateX(-50%);
        }
        .blockquote p{
            font-size: 16px;
        }
        .sec {
            padding: 15px;
            border: 2px dashed var(--secondary);
            margin: 15px 0px;
        }
        /* .sec-01 {
            background: #b5eced;
        }
        .sec-02 {
            background: #b5eced;
        }
        .sec-03 {
            background: #b5eced;
        }
        .sec-04 {
            background: #b5eced;
        }
        .sec-05 {
            background: #b5eced;
        }
        .sec-06 {
            background: #b5eced;
        } */
        
.st0 {
  fill: #ED1C24; }
  .st0:hover {
    fill: #a71116; }

.st1 {
  fill: #8BC349; }
  .st1:hover {
    fill: #5d8a2b; }

.st2 {
  fill: #FFFFFF; }
  .st2:hover {
    fill: #FFFFFF; }

.st3 {
  fill: #428FCD; }
  .st3:hover {
    fill: #2d6fa5; }

.st4 {
  fill: #7A574A; }
  .st4:hover {
    fill: #684436; }

.st5 {
  fill: #5F7E8C; }
  .st5:hover {
    fill: #435b66; }

.st6 {
  fill: #904098; }
  .st6:hover {
    fill: #59225e; }

.st7 {
  fill: #654A9E; }
  .st7:hover {
    fill: #4a3677; }

.st8 {
  fill: #EF4438; }
  .st8:hover {
    fill: #b1352d; }

.st9 {
  fill: #9D9D9D; }
  .st9:hover {
    fill: #696464; }

.st10 {
  fill: #4355A5; }
  .st10:hover {
    fill: #3b4883; }

.st11 {
  fill: #F89820; }
  .st11:hover {
    fill: #ce7f1e; }

.st12 {
  fill: #00A651; }
  .st12:hover {
    fill: #036d36; }

.st13 {
  fill: #16988A; }
  .st13:hover {
    fill: #116960; }

.st14 {
  fill: #FCC115; }
  .st14:hover {
    fill: #af860c; }
#Layer_1 a{
    cursor: pointer;
}
.logo-txt{
    color: #fff;
    font-size: 22px;
    padding: 8px 0;
}
    </style>
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
</head>
<body id="home">
    <header>
        <div class="container">
            <div class="row">
                <div class="res-nav">
                    <i class="fas fa-bars"></i>
                </div>
                <div class="overly"></div>
                <div class="col-lg-2">
                    {{-- <h4 class="logo-txt">ডিজিটাল ইলেকশন ক্যাম্পেইন প্লাটফর্ম </h4> --}}
                    <h4 class="logo-txt">মেজর মোহাম্মদ আলী (অব:)</h4>
                    {{-- <div class="logo"><a href="{{ route('home') }}"><img src="{{ asset('front') }}/images/logo/logo.png" alt=""></a></div> --}}
                </div>
                <div class="col-lg-10">
                    <nav>
                        <ul>
                            {{-- <li><a class="active" href="{{($is_success==true)?'/':'#home'}}">হোম</a></li> --}}
                            <li><a href="{{($is_success==true)?'/':'#plan'}}">মহাপরিকল্পনা</a></li>
                            <li><a href="{{($is_success==true)?'/':'#commitment'}}">কার্যক্রম</a></li>
                            <li><a href="{{($is_success==true)?'/':'#commitment'}}">আকুল আবেদন</a></li>
                            <li><a href="{{($is_success==true)?'/':'#commitment'}}">ব্যক্তিগত পরিচিতি</a></li>
                            <li><a href="{{($is_success==true)?'/':'#commitment'}}">অনলাইন মতামত</a></li>
                            @if (!Auth::guest())
                                <li><a href="{{ url('opinions') }}">অ্যাডমিন</a></li>
                            @endif
                            <li class="hide-m"><a class="btn btn-success mt-3" href="#" data-toggle="modal" data-target="#typeDefineForm">অনলাইন বার্তা পাঠান</a><br><span class="text-light"> অথবা কল করুন ০১৯৩৬৫৫৫৬৬৬</span></li >
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    @yield('content')
    <footer>
        <div class="container">
            <div class="footer-ss">
                <ul class="">
                    <li>
                        <a target="_blank" href="https://www.facebook.com/BarristerSheikhFazleNoorTaposh.official/">
                            <i class="fab fa-facebook" aria-hidden="true"></i>
                        </a>
                    </li>
                    <!-- <li>
                        <a target="_blank" href="#">
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="#">
                            <i class="fab fa-youtube" aria-hidden="true"></i>
                        </a>
                    </li> -->
                </ul>
            </div>
            <div class="copyright">
                <p>
                    <small>Copyright © <?php date('Y') ?> মেজর মোহাম্মদ আলী (অব:)</small><br>
                    <small>All Rights Reserved.
                        এই সাইটটি দাউদকান্দি নির্বাচন ২০২০-এ বাংলাদেশ আওয়ামী লীগের মনোনীত প্রার্থী মেজর মোহাম্মদ আলী (অব:) এর কপিরাইটযুক্ত। আপনি কেবলমাত্র আপনার ব্যক্তিগত, অবাণিজ্যিক ব্যবহারের জন্য অথবা আপনার সংস্থার মধ্যে ব্যবহার করার জন্য এই সাইটের উপাদানসমূহ ডাউনলোড, প্রদর্শন, মুদ্রণ এবং পুনরায় তৈরি করতে পারেন (এই বিজ্ঞপ্তিটি ধরে রেখে)।
                    </small>
                </p>
            </div>
            <!-- <div class="footer-nav">
                <a href="#">Privacy Policy</a> <a href="#">Terms and Conditions</a> <a href="contact.html">Contact us</a>
            </div> -->
        </div>
    </footer>

    <div class="vote" data-toggle="modal" data-target="#helpModal">
        <img src="{{ asset('front') }}/images/logo/votenouka.png" alt="">
    </div>
    {{-- <a target="blank" class="istehaar-float" download href="{{ asset('front/pdf/istehaar.pdf') }}">ইশতেহার ডাউনলোড করুন</a> --}}
    
    <div class="mobile-view">
        <a class="btn flex-fill" data-toggle="modal" data-target="#helpModal"><i class="fas fa-comment"></i> <br> বার্তা পাঠান</a>
        <a class="btn flex-fill" href="tel:01713263435"><i class="fas fa-phone"></i>  <br> কল করুন</a>
        <a class="btn flex-fill"  id="back-to-top-m"><i class="fas fa-arrow-alt-circle-up"></i>  <br> উপরে যান</a>
    </div>
    <div class="hide-m">
        <div class="top_awro pull-right" id="back-to-top" data-toggle="tooltip" data-placement="top" title="Back to top">
            <i class="fas fa-chevron-up" aria-hidden="true"></i>
        </div>
    </div>
    <!-- script -->
    @stack('scripts')
    <script src="{{ asset('front/vendors/jquery/jquery.min.js') }}"></script>
    <!-- <script src="{{ asset('js/main.js') }}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="{{ asset('front/vendors/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('front/vendors/js/wow.min.js') }}"></script>

    <script src="{{ asset('front/vendors/timeline/zoomtimeline.js') }}"></script>
    <link rel='stylesheet' type="text/css" href="{{ asset('front/vendors/timeline/zoombox.css') }}"/>
    <script src="{{ asset('front/vendors/timeline/zoombox.js') }}"></script>
    <script src="{{ asset('front/vendors/timeline/zoomplayer.js') }}"></script>
    <link rel='stylesheet' type="text/css" href="{{ asset('front/vendors/timeline/zoomplayer.css') }}"/>
    <script src="{{ asset('front/vendors/timeline/jquery-ui.min.js') }}"></script>
    <link rel='stylesheet' type="text/css" href="{{ asset('front/vendors/timeline/jquery-ui.min.css') }}"/>
    <script src="{{ asset('front/vendors/timeline/main.js') }}"></script>
    <!-- lightgallery -->
    <script src="{{ asset('front/vendors/lightbox/dist/js/lightgallery.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            // all hide at first time
            $("#type_2").hide();
            $("#type_3").hide();
            $("#type_4").hide();
            $("#type_5").hide();
        });
        // use for show sub type 
        function showSubType(mainType){
            $("#type_1").hide();
            $("#type_2").hide();
            $("#type_3").hide();
            $("#type_4").hide();
            $("#type_5").hide();
            $('#type_'+mainType).show();
        }
    </script>
    <script>
            window.init_zoombox_settings = {
                settings_zoom_doNotGoBeyond1X:'off'
                ,design_skin:'skin-nebula'
                ,settings_enableSwipe:'off'
                ,settings_enableSwipeOnDesktop:'off'
                ,settings_galleryMenu:'dock'
                ,settings_useImageTag:'on'
                ,settings_paddingHorizontal : '100'
                ,settings_paddingVertical : '100'
                ,settings_disablezoom:'off'
                ,settings_transition : 'fade'
                ,settings_transition_out : 'fade'
                ,settings_transition_gallery : 'slide'
                ,settings_disableSocial: 'on'
                ,settings_zoom_use_multi_dimension: 'off'
            };
            jQuery(document).ready(function($){
            
                $( ".slider-for-prev-target" ).each(function(){
                    var _t = $(this);
            
            
                    var min = 150;
                    var max = 300;
                    if(_t.prev().attr('name')=='item_height'){
            
                        max = 400;
                    }
            
                    _t.prev().hide();
            
            
                    _t.slider({
            
                        min: min
                        ,max: max
                        ,slide: function( event, ui ) {
                            $(this).prev().val(ui.value);
                            $(this).prev().trigger('change');
                        }
                    });
            
                    if(_t.prev().val()){
            
            
                        _t.slider('value',_t.prev().val());
                    }
                })
            });
    </script>
    <script>
        new WOW().init();
        $(document).ready(function(){
            
        $('#lightgallery').lightGallery();

            $(".probisu").hide();
            $(".btn-viewmore").click(function(){
                $(".probisu").slideToggle();
            });
            $('#blogCarousel').carousel({
                interval: 5000
            });
            
            $(".res-nav").click(function(){
                $('.overly').toggle();
                $('nav').animate({width: 'toggle'});
            })
            $(".overly").click(function(){
                $('nav').animate({width: 'toggle'});
                $(this).hide();
            });
            $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });
            // scroll body to 0px on click
            $('#back-to-top').on("click", function() {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
            $('#back-to-top-m').on("click", function() {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
    <script>
        var TxtType = function(el, toRotate, period) {
            this.toRotate = toRotate;
            this.el = el;
            this.loopNum = 0;
            this.period = parseInt(period, 10) || 2000;
            this.txt = '';
            this.tick();
            this.isDeleting = false;
        };

        TxtType.prototype.tick = function() {
            var i = this.loopNum % this.toRotate.length;
            var fullTxt = this.toRotate[i];

            if (this.isDeleting) {
            this.txt = fullTxt.substring(0, this.txt.length - 1);
            } else {
            this.txt = fullTxt.substring(0, this.txt.length + 1);
            }

            this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

            var that = this;
            var delta = 100 - Math.random() * 100;

            if (this.isDeleting) { delta /= 2; }

            if (!this.isDeleting && this.txt === fullTxt) {
            delta = this.period;
            this.isDeleting = true;
            } else if (this.isDeleting && this.txt === '') {
            this.isDeleting = false;
            this.loopNum++;
            delta = 500;
            }

            setTimeout(function() {
            that.tick();
            }, delta);
        };
        window.onload = function() {
            var elements = document.getElementsByClassName('typewrite');
            for (var i=0; i<elements.length; i++) {
                var toRotate = elements[i].getAttribute('data-type');
                var period = elements[i].getAttribute('data-period');
                if (toRotate) {
                new TxtType(elements[i], JSON.parse(toRotate), period);
                }
            }
            // INJECT CSS
            var css = document.createElement("style");
            css.type = "text/css";
            css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
            document.body.appendChild(css);
        };
    </script>
    <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::message() !!}
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-156886630-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-156886630-1');
</script>

</body>
</html>