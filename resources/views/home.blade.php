@extends('layouts.app')

@section('content')
<div class="slider">
    {{-- <video class="w-100 hide-m" playsinline autoplay muted loop>
        <source src="{{ asset('front/videos/desktop.mp4') }}"  type="video/mp4">
        Your browser does not support the video tag.
    </video>
    <video class="w-100 show-m" playsinline autoplay muted loop controls>
        <source src="{{ asset('front/videos/mobile.mp4') }}"  type="video/mp4">
        Your browser does not support the video tag.
    </video> --}}
    <div class="bd-example">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{ asset('front/images/slider/1.jpg') }}" class="w-100 hide-m" alt="...">
                    <img src="{{ asset('front/images/slider/1.jpg') }}" class="w-100 show-m" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('front/images/slider/2.jpg') }}" class="w-100 hide-m" alt="...">
                    <img src="{{ asset('front/images/slider/2.jpg') }}" class="w-100 show-m" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('front/images/slider/3.jpg') }}" class="w-100 hide-m" alt="...">
                    <img src="{{ asset('front/images/slider/3.jpg') }}" class="w-100 show-m" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
<section id="call-to-action">
    <div class="container free_consultation">
        <div class="row justify-content-center">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-lg-9 text-center">
                <p class="lead" style="font-size: 18px">সমন্বিত পরিকল্পনায় প্রত্যাশিত</p>
                <span class="typewrite" data-period="2000" data-type='[ "নিরাপদ", "সম্প্রীতির", "স্বপ্নের", "ডিজিটাল" ]'>
                    <span class="wrap"></span>
                </span>
                <p>দাউদকান্দি গড়তে</p>
                <hr>
                <h2>আমি মেজর মোহাম্মদ আলী (অব:)</h2>
                <small>জানতে চাই আপনাদের সকলের চিন্তাভাবনা ও স্বপ্নের কথা।</small>
                <!-- <h4>
                    <span class="mb-3">জানতে চাই</span> <br>
                    <span class="typewrite" data-period="2000" data-type='[ "নিরাপদ", "সম্প্রীতির", "স্বপ্নের", "ডিজিটাল" ]'>
                    <span class="wrap"></span>
                    </span>
                </h4> -->
                
                <p>
                অনলাইনে আপনার মতামত জানাতে ক্লিক করুন এখানে <br>
                অথবা <br>
                কল করুন ০১৯৩৬৫৫৫৬৬৬ - এই নম্বরে। <br>
                <small>
                    *** অনলাইনে মতামত জমাদানের পর মতবিনিময়ের জন্য আপনাকে ফোন-কল করা হতে পারে। ***
                </small>
                </p>            
                {{-- <a class="btn btn-primary btn-outline" data-toggle="modal" data-target="#helpModal">অনলাইন বার্তা পাঠান</a>
                <p>অথবা কল করুন 12346579 (১০-৬)</p>
                <br>

                <p class="text-center">
                    VOTE FOR DHAKA<br>
                    VOTE <img class="boat" src="images/logo/boat.png" alt=""> <br>
                    Together we make DOWNTOWN DHAKA SOUTH
                </p> --}}
            </div>
            {{-- <div class="col-lg-3">
                <h5>Meet with TAPOSH</h5>
                <p>
                    গঠনমূলক ও কার্যকরী মতামত প্রদানকারী হিসেবে সরাসরি সাক্ষাত করুন আমার সাথে।
                    </p>
            </div> --}}
        </div>
    </div>
</section>




<!-- svg map -->
@include('include.map')
<!-- svg map -->









<section class="vision" id="commitment">
    <div class="container">
        <div class="vision-sec wow zoomIn">
            <div class="col-md-12 text-center my-5">
                <h3>মহাপরিকল্পনা</h3>
                <p>অর্থনৈতিকভাবে অগ্রসরমান বাংলাদেশের জন্য প্রতিটি উপজেলা তথা গ্রামে আধুনিক সুবিধা সম্প্রসারণের লক্ষ্যে মাননীয় প্রধানমন্ত্রী যে স্বপ্ন দেখেছেন, সেই স্বপ্নের গ্রাম ও ইউনিয়ন বিনির্মাণে তথা সরকারের ২০২১ ও ২০৪১ এর রূপকল্পে বাংলাদেশকে ডিজিটাল বাংলাদেশে রূপান্তরকরণে আমাদের প্রাণের দাউদকান্দির প্রতিটি গ্রামে নগর সুবিধার প্রসার বাড়ানোর লক্ষ্যে আসন্ন দাউদকান্দি উপজেলা নির্বাচনে আপনাদের সমর্থন ও ভোটে উপজেলা চেয়ারম্যান নির্বাচিত হলে নিম্নবর্ণিত মহাপরিকল্পনা নিয়ে অগ্রসর হতে আমি দৃঢ় প্রতিজ্ঞ।</p>
            </div>
            <div class="sec">
                <div class="row sec-01">
                    <div class="col-md-3">
                        <h4>সবার জন্য 'দ্রুততর স্বাস্থ্যসেবা'</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-0 text-justify">"করোনা মহামারিতে পুরো বিশ্বকে স্বাস্থ্যসেবায় সর্বোচ্চ বেগ পেতে হয়েছে। প্রতিটি দেশ যেভাবে করোনার সাথে সর্বোচ্চ দিয়ে যুদ্ধ করেছে, তার সাপেক্ষে আমাদের দাউদকান্দির স্বাস্থ্যসেবার সাথে সংশ্লিষ্ট কর্মকর্তাদের প্রচেষ্টাও বেশ প্রসংশনীয়। স্বাস্থ্যখাতের নিত্যনতুন সকল চ্যালেঞ্জ মোকাবেলায় দ্রুততার সাথে প্রশাসনিকসহ যাবতীয় স্বাস্থ্যসেবা নিশ্চিতের জন্য মাননীয় প্রধানমন্ত্রী জননেত্রী শেখ হাসিনার ডিজিটাল বাংলাদেশ বিনির্মানের অংশ হিসেবে দাউদকান্দির স্বাস্থ্যসেবা ডিজিটালাইজেশন তথা সবার জন্য দ্রুততর স্বাস্থ্যসেবা নিশ্চিত করা আমার অন্যতম ও প্রধান লক্ষ্য।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী <small>(অব:)</small><br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3">
                        <div class="comment-box">
                            <p>সবার জন্য 'দ্রুততর স্বাস্থ্যসেবা' নিশ্চিত করতে আপনি কি ভাবছেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(2)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="sec">
                <div class="row sec-02">
                    <div class="col-md-3">
                        <h4>শিক্ষাব্যবস্থায় ডিজিটাল অবকাঠামো</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"স্বাস্থ্যসেবার পর করোনা মহামারিতে অন্যতম চ্যালেঞ্জিং বিষয় ছিল আমাদের আগামী ভবিষ্যৎ কান্ডারীদের শিক্ষাদান অব্যাহত রাখা। যেখানে সারাদেশে লকডাউনের ফলে সবকিছু স্থবির হয়ে পড়েছিল, তবুও আমাদের মাননীয় প্রধানমন্ত্রীর   সুদক্ষ নেতৃত্বে চলমান ডিজিটাল বাংলাদেশ গঠনে ভূয়সী অগ্রগতির ফলে অনলাইন শিক্ষা কার্যক্রম প্রাথমিক চ্যালেঞ্জ সফলভাবে মোকাবিলা করতে সক্ষম হয়েছি। যুগের সাথে তাল মিলিয়ে দূরদর্শী নেত্রী মাননীয় প্রধানমন্ত্রী শেখ হাসিনার এবারের লক্ষ্য সমগ্র ডিজিটাল শিক্ষাদান ও মূল্যায়ন ব্যবস্থা তথা সমগ্র ডিজিটাল শিক্ষা অবকাঠামো গঠন। তারই অংশ হিসেবে দাউদকান্দি উপজেলার অধীনস্থ সকল শিক্ষা প্রতিষ্ঠান, শিক্ষক-শিক্ষার্থীদের মাঝে ডিজিটালাইজেশনের সুফল সুনিশ্চিত করা আমার অন্যতম ও প্রধান অঙ্গীকার। 
                                                        তার পাশাপাশি ডিজিটাল শিক্ষাব্যবস্থায় নিজ উদ্যোগে আমার প্রতিজ্ঞা - 'ঘরে ঘরে শিক্ষা' স্লোগানে দাউদকান্দির সকল শিক্ষা প্রতিষ্ঠানের সমন্বয় সাধনের মাধ্যমে দাউদকান্দি উপজেলা অনলাইন লার্নিং প্ল্যাটফর্ম তৈরি করবো ইনশাল্লাহ।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী (অব:)<br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>

                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>শিক্ষাব্যবস্থায় ডিজিটাল অবকাঠামো গড়তে আমাদের কি কি পদক্ষেপ নেয়া উচিত বলে আপনি মনে করেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(3)" data-toggle="modal" data-target="#typeDefineForm" >আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec">
                <div class="row sec-03">
                    <div class="col-md-3">
                        <h4>সমন্বিত উন্নয়ন পরিকল্পনা</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"দাউদকান্দি যেমন আপনার-আমার সবার, এর উন্নয়ন নিয়ে ভাবনাও রয়েছে সবার। আমি বিশ্বাস করি, দশে মিলে যেকোনো কাজ আন্তরিকতার সহিত করলে সে কাজ গতি পায়, সফলতা ও কল্যাণ বয়ে আনে। সে লক্ষ্যে তথা কার্যকর ও জবাবদিহিমূলক স্থানীয় সরকার ব্যবস্থা গঠনে আমি প্রতিজ্ঞাবদ্ধ - সমন্বিত পরিকল্পনায় হবে, দাউদকান্দির উন্নয়ন।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী (অব:)<br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>সমন্বিত উন্নয়ন পরিকল্পনা নিশ্চিত করতে কি কি বিষয় নিয়ে কাজ করা উচিত বলে আপনি মনে করেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(13)" data-toggle="modal" data-target="#typeDefineForm" >আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec">
                <div class="row sec-04">
                    <div class="col-md-3">
                        <h4>রাজনৈতিক উন্নয়ন, সুশাসন ও নাগরিক অধিকার নিশ্চিত</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"নাগরিকের সকল সুযোগ- সুবিধা ও অধিকার সুরক্ষার জন্য প্রয়োজন সুশাসনের। সুশাসনের জন্য প্রয়োজন যোগ্য, দক্ষ, ও অভিজ্ঞ নেতৃত্বর। বিগত সময় ধরে আমরা আওয়ামী লীগ সভাপতি জননেত্রী শেখ হাসিনার নির্দেশনা অনুযায়ী শত চক্রান্ত ও চ্যালেঞ্জের মাঝেও রাজনৈতিক উন্নয়ন, সুশাসন ও নাগরিক অধিকার নিশ্চিত প্রতিষ্ঠা করতে নিরলস কাজ করে গেছি। যারই ধারাবাহিকতায় করোনা মহামারী ও দেশের দুর্যোগকালীন মুহূর্তে আমরা ক্ষমতাসীন আওয়ামী লীগ পরিবার প্রশাসনকে সঙ্গে নিয়ে দলমত, বর্ণ নির্বিশেষে মাননীয় প্রধানমন্ত্রীর আহ্বানে গণমানুষের সহযোগিতায় সংকটকালীন মুহূর্তেও জীবন বাজি রাখতে পিছপা হইনি। এবং রাজনৈতিক উন্নয়নের স্বার্থে জাতি-ধর্ম-বর্ণ-গোত্র নির্বিশেষে নাগরিকদের সামাজিক ও অর্থনৈতিক উন্নয়ন নিশ্চিতে আমৃত্যু কাজ করে যাবো। গণমানুষের কল্যাণ ও অধিকার নিশ্চিত আমার নৈতিক, সামাজিক ও রাজনৈতিক দায়িত্ব।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী (অব:)<br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>রাজনৈতিক উন্নয়ন, সুশাসন ও নাগরিক অধিকার নিশ্চিত করতে আপনার প্রত্যাশা কি?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(4)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec">
                <div class="row sec-05">
                    <div class="col-md-3">
                        <h4>ইউনিয়নভিত্তিক ডিজিটাল সামাজিক নিরাপত্তা বেষ্টনী ও উন্নত দাউদকান্দি</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"সমাজের অসহায়, নির্যাতিত, দুস্থ ও সুবিধাবঞ্চিত জনগোষ্ঠীকে আর্থিক সহায়তাসহ সম্ভাব্য সব ধরনের সহায়তা দেওয়ার ব্যবস্থাই হলো সামাজিক নিরাপত্তা বেষ্টনী। সরকারের দারিদ্র্য বিমোচন কৌশলপত্র (পিআরএসপি) বয়স্ক ভাতা, বিধবা ও দুঃস্থ মহিলা ভাতা, দৈহিক অক্ষমতাসম্পন্ন দুঃস্থ ভাতা, দরিদ্র মাতা ভাতা, যুদ্ধাহত মুক্তিযোদ্ধা ভাতা, অনাথ ভাতা, প্রান্তিক চাষীদের জন্য ভর্তুকি, স্কুল হতে ঝড়ে পড়া রোধে শিক্ষার্থী ভাতাসহ মোট ১৮টি সামাজিক নিরাপত্তা কর্মসূচি চিহ্নিত করেছে এবং সরকারের পক্ষ হতে বিগত প্রায় চার দশক ধরে সেসব আর্থিক সহয়তা প্রদান করা হচ্ছে। তারই ধারাবাহিকপ্তায় এবার উন্নত দাউদকান্দি নিশ্চিত করতে মহাপরিকল্পনার অংশ হিসেবে সামাজিক নিরাপত্তায় সরকারের আর্থিক সহায়তায় ডিজিটালাইজেশনের মাধ্যমে শতভাগ স্বচ্ছতা আনয়নের পাশাপাশি বিভিন্ন উদ্যোক্তা ব্যক্তি ও প্রতিষ্ঠানের সম্পৃক্ততার মাধ্যমে বেসরকারী ভাবে সামাজিক নিরাপত্তা বেষ্টনী প্ল্যাটফর্ম তৈরি করা আমার রাজনৈতিক জীবনের প্রধান ভিশন।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী (অব:) <br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>ইউনিয়নভিত্তিক ডিজিটাল সামাজিক নিরাপত্তা বেষ্টনী ও উন্নত দাউদকান্দি উপহার দিতে আমাদের কি কি বিষয় নিশ্চিত করা উচিত বলে আপনি মনে করেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(5)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="sec">
                <div class="row sec-05">
                    <div class="col-md-3">
                        <h4>আমার দাউদকান্দি, আমার শহর</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"আমার গ্রাম, আমার শহর এর অংশ হিসেবে বাংলাদেশের প্রতিটি গ্রামে আধুনিক নগর সুবিধা সম্প্রসারণের লক্ষ্যে মাননীয় প্রধানমন্ত্রী যে স্বপ্ন দেখেছেন, সেই স্বপ্নের গ্রাম বিনির্মাণে তথা সরকারের ২০২১ ও ২০৪১ এর রূপকল্পে বাংলাদেশকে ডিজিটাল বাংলাদেশে রূপান্তরকরণে আমাদের প্রাণের দাউদকান্দির প্রতিটি গ্রামে নগর সুবিধার প্রসার বাড়ানোর মাধ্যমে দাউদকান্দিবাসীর উন্নত জীবনমান, দারিদ্র দূরীকরণ, টেকসই উন্নয়নের লক্ষ্যমাত্রা অর্জন করতঃ উন্নত দাউদকান্দি গঠন তথা উন্নত বাংলাদেশ গঠন আমার অন্যতম কর্তব্য।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী (অব:) <br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>উন্নত দাউদকান্দি শহর উপহার দিতে আমাদের কি কি বিষয় নিশ্চিত করা উচিত বলে আপনি মনে করেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(6)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec">
                <div class="row sec-05">
                    <div class="col-md-3">
                        <h4>প্রবাসী অনলাইন পোর্টাল ও হেল্পডেস্ক গঠন</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"বৈদেশিক কর্মসংস্থান বাংলাদেশের আর্থ-সামাজিক প্রেক্ষাপটে অত্যন্ত গুরুত্বপূর্ণ। বিদেশে কর্মসংস্থান শুধুমাত্র দেশের বেকারত্ব হ্রাসই করে না, একই সাথে বিদেশে কর্মরত প্রবাসী ভাই-বোনদের প্রেরণকৃত রেমিটেন্স দেশের অর্থনীতির চাকাকে সচল রাখছে। জাতির পিতা বঙ্গবন্ধু শেখ মুজিবুর রহমানের নেতৃত্বে দেশ স্বাধীন হওয়ার পর যুদ্ধবিধ্বস্ত বাংলাদেশের অর্থনীতি পুনরুদ্ধার করার লক্ষ্যে ১৯৭২ সালের ২০ জানুয়ারি শ্রম ও জনশক্তি মন্ত্রণালয় প্রতিষ্ঠা করেন এবং কূটনৈতিক তৎপরতার মাধ্যমে বৈদেশিক কর্মসংস্থান ও কর্মী প্রেরণ বিষয়ে মধ্যপ্রাচ্যের মুসলিম দেশসমূহের সাথে সমঝোতা সৃষ্টি করেন। তারই ধারাবাহিকতায় আজ সারাবিশ্বে আমাদের ভাই-বোনেরা দেশের অর্থনীতিতে গুরুত্বপূর্ণ অবদান রেখে আসছে। যে প্রবাসী ভাই-বোনেরা পরিবার রেখে দূরদেশে পাড়ি দিয়ে দেশের জন্য এত গুরুত্বপূর্ণ অবদান রাখছে, সেই প্রবাসী ও তাদের পরিবারের নিয়মিত খোঁজ-খবর, বিপদে-আপদে পাশা থাকা আমাদের সামাজিক ও নৈতিক দায়িত্ব। এ লক্ষ্যে সরকারের ডিজিটাল হেল্প ডেস্ক - প্রবাসবন্ধু কল সেন্টার এর পাশাপাশি নিজ উদ্যোগে আমাদের দাউদকান্দির প্রবাসী ভাই-বোন ও তাঁদের পরিবারের বিভিন্ন সমস্যা সমাধানের জন্য চালু করবো দাউদকান্দি প্রবাসী অনলাইন পোর্টাল ও হেল্পডেস্ক। "</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী <small>(অব:)</small> <br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>প্রবাসী অনলাইন পোর্টাল ও হেল্পডেস্ক গঠন করতে আমাদের কি কি বিষয় নিশ্চিত করা উচিত বলে আপনি মনে করেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(7)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec">
                <div class="row sec-05">
                    <div class="col-md-3">
                        <h4>সুশৃঙ্খলা ক্রীড়াঙ্গন, মাদক মুক্ত শিক্ষাঙ্গন</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"দেশের ভবিষ্যৎ কান্ডারী তথা আমাদের শিক্ষার্থীদের জন্য সুশৃঙ্খলা শিক্ষাঙ্গন নিশ্চিত রাখা আমাদের সামাজিক কর্তব্য। একইসাথে শারীরিক উন্নতি ও মেধা বিকাশে ক্রীড়ার সাথে সম্পৃক্ততারও কোনো বিকল্প নেই। এজন্য চাই আমাদের শিক্ষার্থী ও যুবাদের খেলাধুলার জন্য পর্যাপ্ত সুযোগ-সুবিধা। শারীরিক, মানসিক উৎকর্ষ সাধন নিশ্চিতের পাশাপাশি মাদকের ভয়ানক ছোবল হতে আমাদের যুব সমাজকে রক্ষা করা আমার সামাজিক ও রাজনৈতিক প্রতিজ্ঞা।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী <small>(অব:)</small> <br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>সুশৃঙ্খলা ক্রীড়াঙ্গন, মাদক মুক্ত শিক্ষাঙ্গন উপহার দিতে আমাদের কি কি বিষয় নিশ্চিত করা উচিত বলে আপনি মনে করেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(8)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec">
                <div class="row sec-05">
                    <div class="col-md-3">
                        <h4>জাতীয় মানের ক্রীড়া একাডেমী প্রতিষ্ঠা</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"আধুনিক  বিশ্বে  বাংলাদেশের নাম দিন দিন প্রস্ফুটিত  করে তোলার  পেছনে আমাদের  ক্রীড়ার  গুরুত্ব  অপরিসীম।  বর্তমানে ক্রীড়ার  জনপ্রিয়তা  ব্যক্তিগত,  সমষ্টিগত  ও জাতিগতভাবে  সমগ্র  বিশ্বকে  একই  সূত্রে  গেঁথে দিয়েছে।  দেশের  মর্যাদা  ও  ভাবমূর্তি বৃদ্ধিতে ক্রীড়া  গুরুত্বপূর্ণ ভূমিকা রেখে  আসছে। বিশ্ব আমাদের চিনছে একটি ক্রীড়ামোদি সম্পৃতির দেশ হিসেবে। আন্তর্জাতিক ক্রীড়াঙ্গনে বাংলাদেশের হয়ে দাউদকান্দি অবদান রাখবে - সেই স্বপ্ন আমার দীর্ঘদিনের। আমার স্বপ্নের মাধ্যমে আমি দাউদকান্দিবাসীকেও আলোড়িত করতে চাই, আমার স্বপ্ন হয়ে উঠুক সবার স্বপ্ন। দাউদকান্দি যেন দেশের ক্রীড়াঙ্গনে অবদান রাখতে পারে সে লক্ষ্যে  পর্যাপ্ত  ক্রীড়া  অবকাঠামোর সুবিধাদি  এবং  সঠিক  প্রশিক্ষণ চাহিদা পূরনের  জন্য প্রাথমিক পর্যায়ে একটি  জাতীয় মানের ক্রীড়া একাডেমী নিশ্চিত করতে চাই, যেখানে উপজেলাবাসীর ক্রীড়া প্রতিভা  সনাক্ত  করণ,  প্রতিভাবান  খেলোয়াড়দের তৈরি হবে, এবং আমাদের দাউদকান্দি তথা দেশের মুখ উজ্জ্বল হবে।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী <small>(অব:)</small> <br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>জাতীয় মানের ক্রীড়া একাডেমী উপহার দিতে আমাদের কি কি বিষয় নিশ্চিত করা উচিত বলে আপনি মনে করেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(9)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec">
                <div class="row sec-05">
                    <div class="col-md-3">
                        <h4>সবুজ দাউদকান্দি</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"প্রতিনিয়ত দেশের জনসংখ্যা ও পরিবেশগত সমস্যা বৃদ্ধি পাচ্ছে। বাতাসে বাড়ছে কার্বন ডাই-অক্সাইডের পরিমাণ এবং একইসাথে জলবায়ু পরিবর্তন আমাদের জন্য আরেকটি চ্যালেঞ্জ। সেজন্য অর্থনৈতিক উন্নতির পাশাপাশি দাউদকান্দির পরিবেশগত উন্নতিও আমাদের অন্যতম লক্ষ্য। আমাদের সবার চাওয়া একটি সবুজ ও মনোরোম পরিবেশ সম্বলিত মনোমুগ্ধকর সবুজ দাউদকান্দি, যেখানে সকল নাগরিক সুযোগ-সুবিধার পাশাপাশি থাকবে প্রাকৃতিক আমেজ আর কর্মচঞ্চলতার পাশাপাশি নান্দনিক উচ্ছলতা।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী <small>(অব:)</small> <br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>সবুজ দাউদকান্দি উপহার দিতে আমাদের কি কি বিষয় নিশ্চিত করা উচিত বলে আপনি মনে করেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(10)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec">
                <div class="row sec-05">
                    <div class="col-md-3">
                        <h4>উপজেলা হাই-টেক পার্ক তৈরি, আইটি ও কারিগরী প্রশিক্ষণের প্রসার, এবং কর্মসংস্থান ব্যাংক তৈরি</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"একুশ শতকের চাহিদা মেটাতে কারিগরী শিক্ষা খাত ও তথ্যপ্রযুক্তিতে দক্ষ জনশক্তি গড়ে তুলতে দেশে বিভিন্ন প্রযুক্তিগত কারিগরী প্রশিক্ষণ এবং হাইটেক আইসিটি পার্ক গড়ছে সরকার। শিক্ষা মন্ত্রণালয়, ডাক, টেলিযোগাযোগ, তথ্য ও প্রযুক্তি মন্ত্রণালয়ের বিভিন্ন প্রকল্প বাস্তবায়ন করে চলেছে মাননীয় প্রধানমন্ত্রী জননেত্রী শেখ হাসিনার সরকার। সেই ধারাবাহিকতায় দাউদকান্দির শিক্ষার্থী ও যুব সমাজের জন্য দাউদকান্দি উপজেলাভিত্তিক আইটি ও কারিগরী প্রশিক্ষণের প্রসারের লক্ষ্যে আমি সরকারী ও বেসরকারী উদ্যোগের সমন্বয়ে ‘সমন্বিত হাই-টেক পার্ক’ গঠন নিশ্চিত করবো এবং একইসাথে কর্মসংস্থান নিশ্চিতকল্পে একটি কমিউনিটি বেইজড কর্মসংস্থান ব্যাংক বা প্ল্যার্টফর্ম তৈরি করবো।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী <small>(অব:)</small> <br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>উপজেলা হাই-টেক পার্ক তৈরি, আইটি ও কারিগরী প্রশিক্ষণের প্রসার, এবং কর্মসংস্থান ব্যাংক তৈরি করতে আমাদের কি কি বিষয় নিশ্চিত করা উচিত বলে আপনি মনে করেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(11)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec">
                <div class="row sec-05">
                    <div class="col-md-3">
                        <h4>টেকসই সামাজিক সম্প্রীতি, শিশু ও নারী নিরাপত্তা জোরদার করণ</h4>
                        <img src="{{ asset('front/images/background/logo.png') }}" alt="image" class="img-fluid">
                    </div>
                    <div class="col-md-6">
                        <blockquote class="blockquote">
                            <p class="mb-2 text-justify">"অসংখ্য মাখলুকাতের মাঝে মানুষ সর্বশ্রেষ্ঠ ও একমাত্র সামাজিক প্রানী। সামাজিক জীব হিসাবে মানুষ সমাজে একে অপরের প্রতি নির্ভরশীল এবং পারস্পারিক সহযোগী। সমাজে যদি সামাজিক সম্প্রীতি বিরাজ না করে, শিশু ও নারীদের নিরাপত্তা না থাকে, মারামারি-হানাহানি ও সাম্প্রদায়িক দাঙ্গা থাকে তাহলে মানব সমাজ এবং হিংস্র জীব জন্তুর মাঝে তেমন পার্থক্য থাকে না। সমাজে জাতি, ধর্ম, বর্ণ, নারী-পুরুষ সকলে মিলে নিরাপদে বসবাস করার জন্য প্রয়োজন রয়েছে সামাজিক সম্প্রীতি ও নিরাপত্তা। সামাজিক সম্প্রীতি ও নিরাপত্তা জোরদার করণ তথা মৌলিক অধিকার নিশ্চিতকরণ, শিশু ও নারী নিরাপত্তা নিশ্চিত, স্ব-স্ব ধর্ম পালন সহ সকলের জীবনের নিরাপত্তা, বসবাসের নিরাপত্তা, অপরাধ কর্ম দমন, নৈতিক শিক্ষা প্রদানে সম্মিলিত ও সামাজিক উদ্যোগ গ্রহণ আমার নৈতিক, সামাজিক ও রাজনৈতিক প্রতিজ্ঞা।"</p>
                            <div class="blockquote-footer">মেজর মোহাম্মদ আলী <small>(অব:)</small> <br>
                                <span class="ml-4">উপজেলা চেয়ারম্যান প্রার্থী, দাউদকান্দি উপজেলা পরিষদ</span>
                            </div>
                        </blockquote>
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="comment-box">
                            <p>টেকসই সামাজিক সম্প্রীতি, শিশু ও নারী নিরাপত্তা জোরদার করতে আমাদের কি কি বিষয় নিশ্চিত করা উচিত বলে আপনি মনে করেন?</p>
                            <a class="btn btn-primary btn-outline" onclick="setType(12)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section class="news-sec bg-img-profile" id="commitment">
    <div class="container">
        <div class="row">
        
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>দাউদকান্দি (উত্তর)</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(2)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>সুন্দলপুর</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(6)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>বারপাড়া</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(14)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>গৌরীপুর</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(7)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>জিংলাতলী</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(5)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>ইলিয়টগঞ্জ (দক্ষিন)</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(4)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>ইলিয়টগঞ্জ (উত্তর)</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(15)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>বিটেশ্বর</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(11)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>মারুকা</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(10)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>দৌলতপুর</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(1)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>মোহাম্মদপুর (পশ্চিম)</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(8)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>মোহাম্মদপুর (পূর্ব)</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(16)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>পদুয়া</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(12)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>গোয়ালমারী</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(9)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
            <div class="col-md-3 text-left">
                <div class="comment-box">
                    <p>পাচঁগাছিয়া (পশ্চিম)</p>
                    <a class="btn btn-primary btn-outline" onclick="setUnion(13)" data-toggle="modal" data-target="#typeDefineForm">আপনার মতামত দিন</a>
                </div>
            </div>
        </div>
    </div>
</section> -->


<section class="news-sec bg-img-profile" id="commitment">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
            <img class="w-100 r-15" src="{{ asset('front/images/profile/major.png') }}" alt="">
            </div>
            <div class="col-md-7">
                <p class="text-justify"><strong>প্রিয় দাউদকান্দিবাসী,</strong>  
                    <br>
                    <br>
                   
                    ডিজিটাল বাংলাদেশ রুপায়নে দাউদকান্দি উপজেলার ডিজিটালাইজেশনের পথ প্রর্দশনকারী চট্টগ্রাম বিভাগের শ্রেষ্ঠ উপজেলা পরিষদ চেয়ারম্যান মেজর মোহাম্মদ আলী (অব:) বিশিষ্ট রাজনৈতিক ব্যক্তিত্ব, সমাজসেবার অঙ্গনে যশস্বী পুরুষ, ১৯৭১ এর বীর মুক্তিযোদ্ধা ও কুমিল্লা-১ (দাউদকান্দি-মেঘনা) আসনের দুই বারের নির্বাচিত সংসদ সদস্য মেজর জেনারেল (অব:) মোহাম্মদ সুবিদ আলী ভূইঁয়া ও বিশিষ্ট শিক্ষানুরাগী মাহমুদা আখতার দম্পতির সুযোগ্য তিন পুত্র সন্তানের মধ্যে জেষ্ঠ পুত্র।
                </p>
	<br>
	এই দাউদকান্দি আমাদের সবার প্রাণের দাউদকান্দি। আমি আশা করি, দাউদকান্দি উপজেলা পরিষদ নির্বাচনে আমাকে নৌকা মার্কায় ভোট দিয়ে উন্নত দাউদকান্দি গড়ে তুলতে সুযোগ দিবেন।

                    <br>
                    <br>
                    শুভেচ্ছা নিরন্তর, অনবরত, অবিরল ধারায় . . . <br><br>
                    <strong class="text-right float-right">
                        মেজর মোহাম্মদ আলী <small>(অব:)</small> <br>	
                        প্রার্থী, দাউদকান্দি উপজেলা পরিষদ
                    </strong>
                </p>
            </div>
        </div>
    </div>
</section>

<section id="gallery">
    <div class="container">
        <div class="page-sec">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Video</h2>
                    <hr>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/jQkbAQGnbpk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="col-lg-4">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/NRmWSgUpIXA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="col-lg-4">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/LMjpRlSHE14" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/Qy2mxB2bfHU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="col-lg-4">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/CTIv_BwyhLE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="col-lg-4">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/VNWpTrz2o1c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <h2>Photo</h2>
                    <hr>
                    <div class="row" id="lightgallery">
                        <a class="col-lg-4" href="{{ asset('front/images/profile/1.jpg') }}">
                            <img src="{{ asset('front/images/profile/1.jpg') }}" />
                        </a>
                        <a class="col-lg-4" href="{{ asset('front/images/profile/2.jpg') }}">
                            <img src="{{ asset('front/images/profile/2.jpg') }}" />
                        </a>
                        <a class="col-lg-4" href="{{ asset('front/images/profile/3.jpg') }}">
                            <img src="{{ asset('front/images/profile/3.jpg') }}" />
                        </a>
                        <a class="col-lg-4" href="{{ asset('front/images/profile/4.jpg') }}">
                            <img src="{{ asset('front/images/profile/4.jpg') }}" />
                        </a>
                        <a class="col-lg-4" href="{{ asset('front/images/profile/5.jpg') }}">
                            <img src="{{ asset('front/images/profile/5.jpg') }}" />
                        </a>
                        <a class="col-lg-4" href="{{ asset('front/images/profile/6.jpg') }}">
                            <img src="{{ asset('front/images/profile/6.jpg') }}" />
                        </a>
                        <a class="col-lg-4" href="{{ asset('front/images/profile/7.jpg') }}">
                            <img src="{{ asset('front/images/profile/7.jpg') }}" />
                        </a>
                        <a class="col-lg-4" href="{{ asset('front/images/profile/8.jpg') }}">
                            <img src="{{ asset('front/images/profile/8.jpg') }}" />
                        </a>
                        <a class="col-lg-4" href="{{ asset('front/images/profile/9.jpg') }}">
                            <img src="{{ asset('front/images/profile/9.jpg') }}" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
</section>
<hr>
<section class="profile">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">ব্যক্তিগত ও পারিবারিক পরিচিতি</h4>
                <br>
                <div class="infinite-content-area infinite-item-pdlr infinite-sidebar-style-none clearfix"><div style="position:absolute;top:0;left:-9999px;">Want create site? Find <a href="http://dlwordpress.com/">Free WordPress Themes</a> and plugins.</div><p>মেজর মোহাম্মদ আলী (অব:)</p>
                <p>তরুন রাজনীতিবিদ, বিশিষ্ট ব্যবসায়ী, ক্রীড়া সংগঠক,<br>
                সাংস্কৃতিমনা, শিক্ষানুরাগী ও সমাজসেবক।<br>
                মোবাইল: ০১৯৩৬৫৫৫৬৬৬<br>
                ইমেইল : <a href="mailto:mdali72@gmail.com">mdali72@gmail.com</a><br>
                ফেসবুক: <a href="https://www.facebook.com/majormohammadalisumon">Major Mohammad Ali – retd</a><br>
                ওয়েবসাইট: <a href="http://mohammadali.info">www.mohammadali.info</a></p>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                    <td style="text-align: left; padding: 10px;">জন্ম</td>
                    <td style="text-align: left; padding: 10px;">&nbsp;১৮ অক্টোবর ১৯৭১, জুরানপুর, দাউদকান্দি, কুমিল্লা।</td>
                    </tr>
                    <tr>
                    <td style="text-align: left; padding: 10px;">পিতা</td>
                    <td style="text-align: left; padding: 10px;">মেজর জেনারেল (অব:) মোঃ সুবিদ আলী ভূঁইয়া,<br>
                    (বীর মুক্তিযোদ্ধা, মাননীয় সংসদ সদস্য ও প্রতিরক্ষা মন্ত্রণালয় সংক্রান্ত সংসদীয় স্থায়ী কমিটির সভাপতি)</td>
                    </tr>
                    <tr>
                    <td style="text-align: left; padding: 10px;">মাতা</td>
                    <td style="text-align: left; padding: 10px;">মাহমুদা আকতার (বিশিষ্ট সমাজসেবিকা ও শিক্ষানুরাগী)</td>
                    </tr>
                    <tr>
                    <td style="text-align: left; padding: 10px;">দাম্পত্য সঙ্গী</td>
                    <td style="text-align: left; padding: 10px;">রুহানী আমরীন (এক্স ক্যাডট – ময়মনসিংহ গার্লস কলেজ)</td>
                    </tr>
                    <tr>
                    <td style="text-align: left; padding: 10px;">সন্তান</td>
                    <td style="text-align: left; padding: 10px;">মৃত: যারিফ আলী (১৯৯৯-২০০৭)<br>
                    শাহির আলী (২০০১)<br>
                    ফাইজা (২০০৯)</td>
                    </tr>
                    <tr>
                    <td style="text-align: left; padding: 10px;">সহোদর</td>
                    <td style="text-align: left; padding: 10px;">শওকত আলী (এক্স ক্যাডেট – মির্জাপুর ক্যাডেট কলেজ, ফাইন্যান্স কনসালট্যান্ট, বৃটেন প্রবাসী)।<br>
                    জুলফিকার আলী (এক্স ক্যাডেট -মির্জাপুর ক্যাডেট কলেজ, আইটি ব্যবসায়ী, আমেরিকা প্রবাসী)।</td>
                    </tr>
                    <tr>
                    <td style="text-align: left; padding: 10px;">ধর্ম</td>
                    <td style="text-align: left; padding: 10px;">ইসলাম</td>
                    </tr>
                    <tr>
                    <td style="text-align: left; padding: 10px;">জাতীয়তা</td>
                    <td style="text-align: left; padding: 10px;">বাংলাদেশী</td>
                    </tr>
                    <tr>
                    <td style="text-align: left; padding: 10px;">রাজনৈতিক দল</td>
                    <td style="text-align: left; padding: 10px;">বাংলাদেশ আওয়ামী লীগ।</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<section class="video-sec">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">প্রত্যাশিত দাউদকান্দি নবসূচনায় আমার ভাবনা</h4>
                <br>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Fy9hsdtEyT4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="timeline-sec" id="plan">
    <div class="bg-timeline">
        <div class="timeline-header wow zoomIn">
            <img src="{{ asset('front/images/profile/taposh-sqr.png') }}" alt="">
            <h4 class="mt-3 plan-headline">ভবিষ্যৎ প্রজন্মের জন্য নিরাপদ দাউদকান্দি গড়তে ৩০ বছর মেয়াদী মহাপরিকল্পনায় যোগ দিন আপনিও।<br>
                আমার নিকট তুলে ধরুন আপনার প্রত্যাশার কথা।</h4>
        </div>

        <section class="mcon-maindemo bgwhite" style="position: relative; padding-top:40px; padding-bottom:50px;">

            <div class="zoomtimeline mode-3dslider auto-init" data-options="{startItem: 2}">
    
                <div class="items bn">
                    <!-- <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ৯০ তম দিন
                            </div>
                        </div>
                        <div class="ztm-content">
                            <button type="button" class="btn btn-info btn-float" onclick="setYear(90)" data-toggle="modal" data-target="#typeDefineForm">আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">মহাপরিকল্পনা প্রণয়ন</h3>
                        </div>
                    </div> -->
                    <!-- <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ২০২০
                            </div>
                        </div>
                        <div class="ztm-content">
                            <button type="button" class="btn btn-info btn-float" onclick="setYear(2020)" data-toggle="modal" data-target="#typeDefineForm">আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">২০২০ সালে আমার নিকট আপনার প্রত্যাশা কি?</h3>
                        </div>
                    </div> -->
                    <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ২০২১
                            </div>
                        </div>
                        <div class="ztm-content">
                        <button type="button" class="btn btn-info btn-float" onclick="setYear(2021)" data-toggle="modal" data-target="#typeDefineForm">আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">২০২১ সালে আমার নিকট আপনার প্রত্যাশা কি?</h3>
                        </div>
                    </div>
                    <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ২০২২ 
                            </div>
                        </div>
                        <div class="ztm-content">
                            <button type="button" class="btn btn-info btn-float"  onclick="setYear(2022)" data-toggle="modal" data-target="#typeDefineForm">আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">২০২২ সালে আমার নিকট আপনার প্রত্যাশা কি?</h3>
                        </div>
                    </div>
                    <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ২০২৩ 
                            </div>
                        </div>
                        <div class="ztm-content">
                        <button type="button" class="btn btn-info btn-float" onclick="setYear(2023)" data-toggle="modal" data-target="#typeDefineForm" >আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">২০২৩ সালে আমার নিকট আপনার প্রত্যাশা কি?</h3>
                        </div>
                    </div>
    
                    <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ২০২৪
                            </div>
                        </div>
                        <div class="ztm-content">
                        <button type="button" class="btn btn-info btn-float" onclick="setYear(2024)" data-toggle="modal" data-target="#typeDefineForm" >আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">২০২৪ সালে আমার নিকট আপনার প্রত্যাশা কি?</h3>
                        </div>
                    </div>
                    <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ২০২৫ 
                            </div>
                        </div>
                        <div class="ztm-content">
                        <button type="button" class="btn btn-info btn-float" onclick="setYear(2025)" data-toggle="modal" data-target="#typeDefineForm" >আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">২০২৫ সালে আমার নিকট আপনার প্রত্যাশা কি?</h3>
                        </div>
                    </div>
                    <!-- <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ২০৩০
                            </div>
                        </div>
                        <div class="ztm-content">
                        <button type="button" class="btn btn-info btn-float" onclick="setYear(2030)" data-toggle="modal" data-target="#typeDefineForm">আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">২০৩০ সালে আমার নিকট আপনার প্রত্যাশা কি?</h3>
                        </div>
                    </div>
                    <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ২০৩৫
                            </div>
                        </div>
                        <div class="ztm-content">
                        <button type="button" class="btn btn-info btn-float" onclick="setYear(2035)" data-toggle="modal" data-target="#typeDefineForm">আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">২০৩৫ সালে আমার নিকট আপনার প্রত্যাশা কি?</h3>
                        </div>
                    </div>
                    <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ২০৪১
                            </div>
                        </div>
                        <div class="ztm-content">
                        <button type="button" class="btn btn-info btn-float" onclick="setYear(2041)" data-toggle="modal" data-target="#typeDefineForm">আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">২০৪১ সালে আমার নিকট আপনার প্রত্যাশা কি?</h3>
                        </div>
                    </div>
                    <div class="ztm-item" data-image="{{ asset('front/images/profile/major.png') }}">
                        <div class="hex-desc-con">
                            <div class="hex-desc">
                                ২০৫০
                            </div>
                        </div>
                        <div class="ztm-content">
                        <button type="button" class="btn btn-info btn-float" onclick="setYear(2050)" data-toggle="modal" data-target="#typeDefineForm" >আপনার প্রত্যাশা</button>
                            <h3 class="the-heading">২০৫০ সালে আমার নিকট আপনার প্রত্যাশা কি?</h3>
                        </div>
                    </div> -->
                </div>
                <!-- the preloader START -->
                <div class="preloader-wave">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- the preloader END -->
            </div>
        </section>
    </div>
</section>

<section class="aboutus-area">
    <div class="container">
        <div class="affiliate">
            <div class="row">
                <div class="col-md-6">
                    <div class="box wow bounceIn">
                        <img src="{{ asset('front/images/timeline/al.png') }}" alt="">
                        <h4>Bangladesh Awami League</h4>
                        <a href="http://www.albd.org/" target="blank">www.albd.org</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box wow bounceIn">
                        <img src="{{ asset('front/images/logo/bd.png') }}" alt="">
                        <h4>মেজর মোহাম্মদ আলী (অব:)</h4>
                        <a href="https://www.mohammadali.info/" target="blank">www.mohammadali.info</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('include.common_form')
@include('include.type_define_form')
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('video').play();
    });
    function setType(arg){
        console.log(arg);
        $("#motamot").show();
        // $("#opinoin_type").val(arg);
        $("#type").val(arg);
        $("#c_type").val(arg);

        // $('#subtype').html('');
        /* get sub type*/
        // $.ajax({
        //     type: "get",
        //     url: BASE_URL + "/get-subtype",
        //     data: {
        //         "_token": '<?php echo csrf_token() ?>',
        //         "type": arg
        //     },
        //     contentType: "application/json;charset=utf-8",
        //     dataType: 'json',
        //     success: function (response_data) {
        //         $.each(response_data, function(key) {
        //             if(key ==0){
        //                 $('#subtype').append('<div class="radio"><label><input type="radio" name="sub_type" value="' + response_data[key].id + '" checked> ' + response_data[key].name + '</label></div>');
        //             } else {
        //                 $('#subtype').append('<div class="radio"><label><input type="radio" name="sub_type" value="' + response_data[key].id + '" > ' + response_data[key].name + '</label></div>');
        //             } 
        //         });
        //     },
        //     // error response
        //     error: function () {
        //         $('#subtype_error').html('');
        //     }
        // });
    }
    function setYear(arg){
        $("#motamot").hide();
        $("#opinoin_year").val(arg);
        $("#opinoin_type").val(1);
    } 

    function setUnion(arg, test=null){
        console.log(arg);
        console.log(test);
        $("#motamot").show();
        // $("#opinoin_type").val(arg);
        $("#union").val(arg);
    }
</script>
@endpush