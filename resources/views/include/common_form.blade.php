<!-- common modal -->
<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('opinion') }}" method="post">
                @csrf
                <div class="modal-header bg-success">
                    <h5 class="modal-title" id="exampleModalLabel">আপনার মতামত জমা দিন</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">নাম<sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="নাম" required>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="phone">মোবাইল নং<sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" id="phone" name="phone" onblur="checkPhone()" max="11" placeholder="মোবাইল নং" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="email">ই-মেইল <span class="text-muted">(ঐচ্ছিক)</span></label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="example@gmail.com">
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="form-group">
                            <label for="type">লিঙ্গ<sup class="text-danger">*</sup></label>
                            <div class="radio">
                                <label><input type="radio" name="gender" value="1" checked> পুরুষ</label>
                                <label><input type="radio" name="gender" value="2"> নারী</label>
                                <label><input type="radio" name="gender" value="3"> অন্যান্য</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="age">বয়স</label>
                                <input type="number" min="0" class="form-control" id="age" name="age" placeholder="বয়স">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="profession">পেশা</label>
                                <input type="text" class="form-control" id="profession" name="profession" placeholder="পেশা">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="ward">ইউনিয়ন</label>
                                <!-- <input type="number" class="form-control" id="ward" name="ward" placeholder="ওয়ার্ড"> -->
                                <select  class="form-control" name="union">
                                    <option value="3">দাউদকান্দি পৌরসভা</option>
                                    <option value="2">দাউদকান্দি (উত্তর)</option>
                                    <option value="6">সুন্দলপুর</option>
                                    <option value="14">বারপাড়া</option>
                                    <option value="7">গৌরীপুর</option>
                                    <option value="5">জিংলাতলী</option>
                                    <option value="4">ইলিয়টগঞ্জ (দক্ষিন)</option>
                                    <option value="15">ইলিয়টগঞ্জ (উত্তর)</option>
                                    <option value="11">বিটেশ্বর</option>
                                    <option value="10">মারুকা</option>
                                    <option value="1">দৌলতপুর</option>
                                    <option value="8">মোহাম্মদপুর (পশ্চিম)</option>
                                    <option value="16">মালিগাঁও</option>
                                    <option value="12">পদুয়া</option>
                                    <option value="9">গোয়ালমারী</option>
                                    <option value="13">পাচঁগাছিয়া (পশ্চিম)</option>
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address">ঠিকানা</label>
                        <textarea name="address" class="form-control" id="address" name="address" rows="2" placeholder="ঠিকানা"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="type">টপিকস এর ধরণ<sup class="text-danger">*</sup></label>
                        <select class="form-control" name="type" id="c_type" required>
                            @foreach($types as $main_key => $type)
                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                        <!-- @foreach($types as $main_key => $type)
                            @if ($type->id==1) @continue; @endif -->
                            <!-- <div class="radio">
                                <label><input type="radio" name="type" value="{{$type->id}}" {{($type->id==2)?'checked':'' }} onclick="showSubType({{$main_key}});"> {{ $type->name }}</label>
                            </div> -->
                            <!-- <div class="ml-4" id="type_{{$main_key}}">
                                @foreach(hybrid_get('opinion_type_subs','opinion_type_id',$type->id,'*') as $sub_key => $sub_type)
                                <div class="radio">
                                    <label><input type="radio" name="sub_type" value="{{ $sub_type->id }}" {{ ($main_key==1 && $sub_key==0)?'checked':'' }}> {{ $sub_type->name }}</label>
                                </div>
                                @endforeach
                            </div> -->
                        <!-- @endforeach -->
                    </div>
                    <div class="form-group">
                        <label for="opinion">আপনার মতামত<sup class="text-danger">*</sup></label>
                        <textarea class="form-control" id="opinion"  name="opinion" rows="6" placeholder="আপনার মতামত লিখুন" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">কেটে দিন</button>
                    <button type="submit" class="btn btn-success">মতামত জমা দিন</button>
                </div>
            </form>
        </div>
    </div>
</div>